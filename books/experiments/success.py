# -*- coding: utf-8 -*-
from __future__ import print_function, division
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import ElasticNet
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import explained_variance_score
from sklearn.metrics import f1_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import median_absolute_error
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import precision_score
from sklearn.metrics import r2_score
from sklearn.metrics import recall_score
from sklearn.metrics import roc_auc_score
from sklearn.svm import LinearSVC

from experiments import get_classifiers
from experiments.utils import output_fname_generator, print_classification_report
from features.utils import fetch_features_vectorized
import os
from helpers.file_helpers import stdout_redirector
from manage import app
import numpy as np
import tqdm



def balanced_accuracy(Y_actual, Y_predicted):

    return recall_score(Y_actual, Y_predicted, pos_label=None, average='macro')


def print_predictions(Y_test, Y_predictions):
    print('*' * 25)
    print('Actual, Predicted')
    for x, y in zip(Y_test.flatten(), np.array(Y_predictions).flatten()):
        print("{}, {}".format(x, y))

    print('*' * 25)


def get_prediction(corpus,feature,dump_dir):
    X_train, Y_train, X_test, Y_test = fetch_features_vectorized(dump_dir, feature, corpus)
    print("Running Experiment for Feature: {}".format(feature))
    print("============================================================")
    print("Training size: {}".format(len(Y_train)))
    print("Test size: {}".format(len(Y_test)))
    print("X Train Feature matrix {}".format(X_train.shape))
    print("X Test Feature matrix {}".format(X_test.shape))

    # svm = LinearSVC(random_state=1234)
    svm = LinearSVC()

    param_grid = {'C': [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1, 10, 100, 1000, 10000]}

    # Train a SVM classification model

    print("Fitting the classifier to the training set")

    grid = GridSearchCV(svm, param_grid, scoring='f1_weighted')
    grid.fit(X_train, Y_train)

    print("Best score: %0.3f" % grid.best_score_)
    print("Best parameters set:")
    best_parameters = grid.best_estimator_.get_params()
    for param_name in sorted(param_grid.keys()):
        print("\t%s: %r" % (param_name, best_parameters[param_name]))

    for params, mean_score, scores in grid.grid_scores_:
        print("%0.3f+/-%0.2f %r" % (mean_score, scores.std() / 2, params))

    print("Done grid search")
    print("============================================================")

    print("Predicting on the test set")

    y_train_pred = grid.predict(X_train)

    y_pred = grid.predict(X_test)

    target_names = ['failure', 'success']
    class_indices = {'failure': 0, 'success': 1}
    train_acc = accuracy_score(Y_train, y_train_pred) * 100
    test_acc = accuracy_score(Y_test, y_pred) * 100

    print("Classifation Report")
    print("Training Accuracy ={}".format(train_acc))
    print("Test Accuracy ={}".format(test_acc))

    print(classification_report(Y_test, y_pred, target_names=target_names,
                                labels=[class_indices[cls] for cls in target_names]))

    y_pred_correct = [int(pred==actual) for pred, actual in zip(y_pred, [class_indices[cls] for cls in target_names])]

    return y_pred, y_pred_correct


def success_classification(corpus, feature, dump_dir,ignore_lst):
    '''

    :param corpus: Dataset
    :param feature:
    :param dump_dir:
    :return:
    '''

    classifiers=['lr','rf','percepton','svc']

    for classifier in tqdm.tqdm(classifiers):
            print (classifier)

            clf,param_grid=get_classifiers(classifier)

            out_file = output_fname_generator(feature)
            out_file= '{}_{}.txt'.format(out_file,classifier)
            with open(os.path.join(app.SUCCESS_OUTPUT, out_file ), 'w') as f:
                with stdout_redirector(f):

                    X_train, Y_train, X_test, Y_test = fetch_features_vectorized(dump_dir, feature, corpus, ignore_lst)
                    print("Running Experiment for Feature: {}".format(feature))
                    print("============================================================")
                    print("Training size: {}".format(len(Y_train)))
                    print("Test size: {}".format(len(Y_test)))
                    print("X Train Feature matrix {}".format(X_train.shape))
                    print("X Test Feature matrix {}".format(X_test.shape))

                    # svm = LinearSVC(random_state=1234)
                    #svm = LinearSVC()

                    #param_grid = {'C': [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1, 10, 100, 1000, 10000]}

                    # Train a SVM classification model

                    print("Fitting the classifier to the training set")

                    grid = GridSearchCV(clf, param_grid, scoring='f1_weighted')
                    grid.fit(X_train, Y_train)

                    print("Best score: %0.3f" % grid.best_score_)
                    print("Best parameters set:")
                    best_parameters = grid.best_estimator_.get_params()
                    for param_name in sorted(param_grid.keys()):
                        print("\t%s: %r" % (param_name, best_parameters[param_name]))

                    for params, mean_score, scores in grid.grid_scores_:
                        print("%0.3f+/-%0.2f %r" % (mean_score, scores.std() / 2, params))

                    print("Done grid search")
                    print("============================================================")

                    print("Predicting on the test set")

                    y_train_pred = grid.predict(X_train)

                    y_pred = grid.predict(X_test)

                    target_names = ['failure', 'success']
                    class_indices = {'failure': 0, 'success': 1}
                    train_acc = accuracy_score(Y_train, y_train_pred) * 100
                    test_acc = accuracy_score(Y_test, y_pred) * 100

                    print("Classifation Report")
                    print("Training Accuracy ={}".format(train_acc))
                    print("Test Accuracy ={}".format(test_acc))

                    print(classification_report(Y_test, y_pred, target_names=target_names,
                                                labels=[class_indices[cls] for cls in target_names]))

                    print("")
                    print("Confusion matrix")
                    print("============================================================")
                    print(target_names)
                    print()
                    print(confusion_matrix(
                        Y_test,
                        y_pred,
                        labels=[class_indices[cls] for cls in target_names]))

                    p_micro, r_micro, f_micro, _ = precision_recall_fscore_support(Y_test, y_pred, average='micro',
                                                                                   pos_label=None)
                    p_macro, r_macro, f_macro, _ = precision_recall_fscore_support(Y_test, y_pred, average='macro',
                                                                                   pos_label=None)
                    p_weighted, r_weighted, f_weighted, _ = precision_recall_fscore_support(Y_test, y_pred, average='weighted',
                                                                                            pos_label=None)
                    roc_auc = roc_auc_score(y_true=Y_test, y_score=y_pred)

                    # Precision recall f1 score for each class : success  and failure
                    # For success
                    precision_success = precision_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)
                    recall_success = recall_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)
                    f1_success = f1_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)

                    # For Failure
                    precision_failure = precision_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)
                    recall_failure = recall_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)
                    f1_failure = f1_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)
                    bac=balanced_accuracy(Y_test,y_pred)

                    print_predictions(Y_test,y_pred)

                    print("")
                    print(
                        "Accuracy,BAC,ROC_AUC,Precision-Macro,Precision-Micro,Precision-Weighted,Recall-Macro,Recall-Micro,Recall-Weighted,F1-Macro,F1-Micro,F1-Weighted,Failure-Precision,Failure-Recall,Failure-f1,Success-Precision,Success-Recall,Success-F1")
                    print(
                        "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}".format(test_acc,bac, roc_auc, p_macro, p_micro,
                                                                                    p_weighted, r_macro, r_micro, r_weighted,
                                                                                    f_macro, f_micro, f_weighted,
                                                                                    precision_failure, recall_failure,
                                                                                    f1_failure, precision_success,
                                                                                    recall_success, f1_success))


def success_3_class_classification(corpus, feature, dump_dir):
    '''

    :param corpus: Dataset
    :param feature:
    :param dump_dir:
    :return:
    '''

    out_file = output_fname_generator(feature)
    with open(os.path.join(app.SUCCESS_OUTPUT_3_CLASS, out_file + '.txt'), 'w') as f:
        with stdout_redirector(f):

            X_train, Y_train, X_test, Y_test = fetch_features_vectorized(dump_dir, feature, corpus)
            print("Running Experiment for Feature: {}".format(feature))
            print("============================================================")
            print("Training size: {}".format(len(Y_train)))
            print("Test size: {}".format(len(Y_test)))
            print("X Train Feature matrix {}".format(X_train.shape))
            print("X Test Feature matrix {}".format(X_test.shape))

            svm = LinearSVC(random_state=1234)

            param_grid = {'C': [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1, 10, 100, 1000, 10000]}

            # Train a SVM classification model

            print("Fitting the classifier to the training set")

            grid = GridSearchCV(svm, param_grid, scoring='f1_weighted')
            grid.fit(X_train, Y_train)

            print("Best score: %0.3f" % grid.best_score_)
            print("Best parameters set:")
            best_parameters = grid.best_estimator_.get_params()
            for param_name in sorted(param_grid.keys()):
                print("\t%s: %r" % (param_name, best_parameters[param_name]))

            for params, mean_score, scores in grid.grid_scores_:
                print("%0.3f+/-%0.2f %r" % (mean_score, scores.std() / 2, params))

            print("Done grid search")
            print("============================================================")

            print("Predicting on the test set")

            y_train_pred = grid.predict(X_train)

            y_pred = grid.predict(X_test)

            target_names = ['failure', 'mild_success','success']
            class_indices = {'failure': 0,'mild_success':1, 'success': 2}
            train_acc = accuracy_score(Y_train, y_train_pred) * 100
            test_acc = accuracy_score(Y_test, y_pred) * 100

            print("Classifation Report")
            print ("Training classification report")

            print(classification_report(Y_train, y_train_pred, target_names=target_names,
                                        labels=[class_indices[cls] for cls in target_names]))

            print(confusion_matrix(
                Y_train,
                y_train_pred,
                labels=[class_indices[cls] for cls in target_names]))


            print("Training Accuracy ={}".format(train_acc))
            print("Test Accuracy ={}".format(test_acc))

            print(classification_report(Y_test, y_pred, target_names=target_names,
                                        labels=[class_indices[cls] for cls in target_names]))

            print("")
            print("Confusion matrix")
            print("============================================================")
            print(target_names)
            print()
            print(confusion_matrix(
                Y_test,
                y_pred,
                labels=[class_indices[cls] for cls in target_names]))

            p_micro, r_micro, f_micro, _ = precision_recall_fscore_support(Y_test, y_pred, average='micro',
                                                                           pos_label=None)
            p_macro, r_macro, f_macro, _ = precision_recall_fscore_support(Y_test, y_pred, average='macro',
                                                                           pos_label=None)
            p_weighted, r_weighted, f_weighted, _ = precision_recall_fscore_support(Y_test, y_pred, average='weighted',
                                                                                    pos_label=None)
            #roc_auc = roc_auc_score(y_true=Y_test, y_score=y_pred)

            # Precision recall f1 score for each class : success  and failure
            # For success
            precision_success = precision_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=2)
            recall_success = recall_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=2)
            f1_success = f1_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=2)



            #For mild succcess

            precision_mild_success = precision_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)
            recall_mild_success = recall_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)
            f1_mild_success = f1_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)


            # For Failure
            precision_failure = precision_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)
            recall_failure = recall_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)
            f1_failure = f1_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)

            print("")
            print(
                "Accuracy,Precision-Macro,Precision-Micro,Precision-Weighted,Recall-Macro,Recall-Micro,Recall-Weighted,F1-Macro,F1-Micro,F1-Weighted,Failure-Precision,Failure-Recall,Failure-f1,Mild Success-Precision,Mild Success-Recall,Mild Success-F1,Success-Precision,Success-Recall,Success-F1")
            print(
                "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}".format(test_acc, p_macro, p_micro,
                                                                            p_weighted, r_macro, r_micro, r_weighted,
                                                                            f_macro, f_micro, f_weighted,
                                                                            precision_failure, recall_failure,
                                                                            f1_failure,precision_mild_success,
                                                                            recall_mild_success, f1_mild_success, precision_success,
                                                                            recall_success, f1_success))




def success_regression(corpus, feature, dump_dir):
    out_file = output_fname_generator(feature)
    with open(os.path.join(app.SUCCESS_REG_OUTPUT, out_file + '.txt'), 'w') as f:
        with stdout_redirector(f):
            X_train, Y_train, X_test, Y_test = fetch_features_vectorized(dump_dir, feature, corpus)
            print("Running Regression Experiment for Feature: {}".format(feature))
            print("============================================================")
            print("Training size: {}".format(len(Y_train)))
            print (Y_train)
            print("Test size: {}".format(len(Y_test)))
            print("X Train Feature matrix {}".format(X_train.shape))
            print("X Test Feature matrix {}".format(X_test.shape))

            # Elastic Regression Experiment parameter setup
            alpha = 0.1
            param_grid = {'l1_ratio': [0.01, 0.05, 0.25, 0.5, 0.75, 0.95, 0.99]}

            regressor = ElasticNet(alpha=alpha, l1_ratio=0.01,random_state=1234)

            best_regressor = GridSearchCV(regressor, param_grid, verbose=1, n_jobs=1)
            best_regressor.fit(X_train, Y_train)

            print("best parameter: ", best_regressor.best_params_)
            print("best score: ", best_regressor.best_score_)

            for params, mean_score, scores in best_regressor.grid_scores_:
                print("%0.3f (+/-%0.03f) for %r"
                      % (mean_score, scores.std() * 2, params))
                print()

            print("Done grid search")

            print("============================================================")

            print("Regression estimation on the test set")

            Y_train_pred = best_regressor.predict(X_train)

            Y_predicted = best_regressor.predict(X_test)

            print("Done with experiment")

            # print regression report

            # TODO MAPE

            rmse_train = np.sqrt(mean_squared_error(Y_train, Y_train_pred))
            print('[EN {}] RMSE on training: {}'.format(best_regressor.best_params_['l1_ratio'], rmse_train))
            r2_train = r2_score(Y_train, Y_train_pred)
            print('[EN {}] R2 on training: {}'.format(best_regressor.best_params_['l1_ratio'], r2_train))

            rmse_test = np.sqrt(mean_squared_error(Y_test, Y_predicted))
            print('[EN {}] RMSE on testing: {}'.format(best_regressor.best_params_['l1_ratio'], rmse_test))
            r2_test = r2_score(Y_test, Y_predicted)
            print('[EN {}] R2 on testing: {}'.format(best_regressor.best_params_['l1_ratio'], r2_test))


            mse_test= mean_squared_error(Y_test, Y_predicted)
            print('Mean Square Error train: %.3f, test: %.3f' % (
                mean_squared_error(Y_train, Y_train_pred),
                mse_test))

            mae_test=mean_absolute_error(Y_test, Y_predicted)
            print('Mean Absolute Error train: %.3f, test: %.3f' % (
                mean_absolute_error(Y_train, Y_train_pred),
                mae_test))

            meae_test=median_absolute_error(Y_test, Y_predicted)
            print('Median Absolute Error train: %.3f, test: %.3f' % (
                median_absolute_error(Y_train, Y_train_pred),
                meae_test))


            evs_test= explained_variance_score(Y_test, Y_predicted)
            print('Explained variance regression score train: %.3f, test: %.3f' % (
                explained_variance_score(Y_train, Y_train_pred),
                evs_test))

            # categorize the scores to classes
            print("Classifation Report")
            target_names = ['failure', 'success']
            class_indices = {'failure': 0, 'success': 1}
            print("============================================================")

            print("Training Report")
            train_acc = accuracy_score(Y_train >= 3.5, Y_train_pred >= 3.5) * 100

            print("Training Accuracy ={}".format(train_acc))

            print(classification_report(Y_train >= 3.5, Y_train_pred >= 3.5, target_names=target_names,
                                        labels=[class_indices[cls] for cls in target_names]))

            print_classification_report(Y_test >= 3.5, Y_predicted >= 3.5, target_names)

            print(
                "RMSE,R2,MSE,MAE,MeAE,EVR")
            print(
                "{},{},{},{},{},{}".format(rmse_test,r2_test,mse_test,mae_test,meae_test,evs_test))
            #
            # from pprint import pprint
            # print ("Training" )
            # pprint(zip([book.book_id for book in corpus.X_train],Y_train,Y_train_pred))
            # print()
            # print ("Testing")
            # # pprint(zip([book.book_id for book in corpus.X_test], Y_train, Y_train_pred))





def success_classification_multitask(corpus, feature, dump_dir,ignore_lst):
    '''

    :param corpus: Dataset
    :param feature:
    :param dump_dir:
    :return:
    '''



    def transform_labels(Y):
        return np.array([ int(label[-1]) for label in Y])



    classifiers=['lr','rf','percepton','svc']

    for classifier in tqdm.tqdm(classifiers):
            print (classifier)

            clf,param_grid=get_classifiers(classifier)

            out_file = output_fname_generator(feature)
            out_file= '{}_{}.txt'.format(out_file,classifier)
            with open(os.path.join(app.SUCCESS_OUTPUT_MT, out_file ), 'w') as f:

                with stdout_redirector(f):

                    X_train, Y_train, X_test, Y_test = fetch_features_vectorized(dump_dir, feature, corpus,ignore_lst)
                    print("Running Experiment for Feature: {}".format(feature))
                    print("============================================================")
                    print("Training size: {}".format(len(Y_train)))
                    print("Test size: {}".format(len(Y_test)))
                    print("X Train Feature matrix {}".format(X_train.shape))
                    print("X Test Feature matrix {}".format(X_test.shape))

                    # svm = LinearSVC(random_state=1234)
                    #svm = LinearSVC(random_state=1234)

                    #param_grid = {'C': [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1, 10, 100, 1000, 10000]}

                    # Train a SVM classification model

                    print("Fitting the classifier to the training set")

                    grid = GridSearchCV(clf, param_grid, scoring='f1_weighted')
                    grid.fit(X_train, Y_train)

                    print("Best score: %0.3f" % grid.best_score_)
                    print("Best parameters set:")
                    best_parameters = grid.best_estimator_.get_params()
                    for param_name in sorted(param_grid.keys()):
                        print("\t%s: %r" % (param_name, best_parameters[param_name]))

                    for params, mean_score, scores in grid.grid_scores_:
                        print("%0.3f+/-%0.2f %r" % (mean_score, scores.std() / 2, params))

                    print("Done grid search")
                    print("============================================================")

                    print("Predicting on the test set")

                    y_train_pred = grid.predict(X_train)

                    y_pred = grid.predict(X_test)

                    target_names = np.unique(Y_train).tolist()
                    # print (target_names)
                    class_indices = { label:i for i,label in enumerate(target_names)}
                    # print (class_indices)
                    train_acc = accuracy_score(Y_train, y_train_pred) * 100
                    test_acc = accuracy_score(Y_test, y_pred) * 100

                    print("Classifation Report")
                    print("Training Accuracy ={}".format(train_acc))
                    print("Test Accuracy ={}".format(test_acc))

                    print(classification_report(Y_test, y_pred, target_names=target_names))

                    print("")
                    print("Confusion matrix")
                    print("============================================================")
                    print(target_names)
                    print()
                    print(confusion_matrix(
                        Y_test,
                        y_pred))

                    p_micro, r_micro, f_micro, _ = precision_recall_fscore_support(Y_test, y_pred, average='micro',
                                                                                   pos_label=None)
                    p_macro, r_macro, f_macro, _ = precision_recall_fscore_support(Y_test, y_pred, average='macro',
                                                                                   pos_label=None)
                    p_weighted, r_weighted, f_weighted, _ = precision_recall_fscore_support(Y_test, y_pred, average='weighted',
                                                                                            pos_label=None)
                    # roc_auc = roc_auc_score(y_true=Y_test, y_score=y_pred)



                    print("")
                    print(
                        "Accuracy,Precision-Macro,Precision-Micro,Precision-Weighted,Recall-Macro,Recall-Micro,Recall-Weighted,F1-Macro,F1-Micro,F1-Weighted")
                    print(
                        "{},{},{},{},{},{},{},{},{},{}".format(test_acc, p_macro, p_micro,
                                                                                    p_weighted, r_macro, r_micro, r_weighted,
                                                                                    f_macro, f_micro, f_weighted))



                    #print result for success class only

                    print("============================================================")
                    print("")
                    print("")


                    target_names = ['failure', 'success']
                    class_indices = {'failure': 0, 'success': 1}

                    Y_train=transform_labels(Y_train)
                    y_train_pred=transform_labels(y_train_pred)
                    Y_test=transform_labels(Y_test)
                    y_pred=transform_labels(y_pred)


                    train_acc = accuracy_score(Y_train, y_train_pred) * 100
                    test_acc = accuracy_score(Y_test, y_pred) * 100

                    print("Classifation Report")
                    print("Training Accuracy ={}".format(train_acc))
                    print("Test Accuracy ={}".format(test_acc))

                    print(classification_report(Y_test, y_pred, target_names=target_names,
                                                labels=[class_indices[cls] for cls in target_names]))

                    print("")
                    print("Confusion matrix")
                    print("============================================================")
                    print(target_names)
                    print()
                    print(confusion_matrix(
                        Y_test,
                        y_pred,
                        labels=[class_indices[cls] for cls in target_names]))

                    p_micro, r_micro, f_micro, _ = precision_recall_fscore_support(Y_test, y_pred, average='micro',
                                                                                   pos_label=None)
                    p_macro, r_macro, f_macro, _ = precision_recall_fscore_support(Y_test, y_pred, average='macro',
                                                                                   pos_label=None)
                    p_weighted, r_weighted, f_weighted, _ = precision_recall_fscore_support(Y_test, y_pred, average='weighted',
                                                                                            pos_label=None)
                    roc_auc = roc_auc_score(y_true=Y_test, y_score=y_pred)

                    # Precision recall f1 score for each class : success  and failure
                    # For success
                    precision_success = precision_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)
                    recall_success = recall_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)
                    f1_success = f1_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)

                    # For Failure
                    precision_failure = precision_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)
                    recall_failure = recall_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)
                    f1_failure = f1_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)


                    print_predictions(Y_test,y_pred)

                    print("")
                    print(
                        "Accuracy,ROC_AUC,Precision-Macro,Precision-Micro,Precision-Weighted,Recall-Macro,Recall-Micro,Recall-Weighted,F1-Macro,F1-Micro,F1-Weighted,Failure-Precision,Failure-Recall,Failure-f1,Success-Precision,Success-Recall,Success-F1")
                    print(
                        "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}".format(test_acc, roc_auc, p_macro, p_micro,
                                                                                    p_weighted, r_macro, r_micro, r_weighted,
                                                                                    f_macro, f_micro, f_weighted,
                                                                                    precision_failure, recall_failure,
                                                                                    f1_failure, precision_success,
                                                                                    recall_success, f1_success))


