from collections import defaultdict
from contextlib import redirect_stdout

from keras import callbacks, Input
from keras.layers import Embedding, Dense, Dropout
from sklearn.metrics import f1_score, classification_report, confusion_matrix, accuracy_score, \
    precision_recall_fscore_support, roc_auc_score, precision_score, recall_score
from sklearn.model_selection import ParameterGrid
import numpy as np
import os
from keras import initializers
from keras.layers.merge import Concatenate

from deep_learning.layers import BilinearLayer, AttentionMLP
from deep_learning.utils import get_optimizers
from keras.models import Model


def balanced_accuracy(Y_actual, Y_predicted):
    return recall_score(Y_actual, Y_predicted, pos_label=None, average='macro')


class Experiemnt:
    def __init__(self, train_Xs, train_Ys, val_Xs, val_Ys, test_Xs, test_Ys, le, params, name):
        self.train_Xs = train_Xs
        self.train_Ys = train_Ys
        self.test_Xs = test_Xs
        self.test_Ys = test_Ys
        self.val_Xs = val_Xs
        self.val_Ys = val_Ys
        self.le = le
        self.params = params
        self.name = name

    def grid_search(self):
        result_gs = defaultdict(list)

        for settings in ParameterGrid(self.params):
            model = self.create_model(settings)
            model.summary()
            earlystop_cb = callbacks.EarlyStopping(monitor='val_loss', patience=settings['patience'], verbose=0,
                                                   mode='auto')
            model.fit(self.train_Xs, self.train_Ys,
                      epochs=settings['epochs'],
                      batch_size=settings['batch_size'],
                      validation_data=(self.val_Xs, self.val_Ys),
                      shuffle=True,
                      callbacks=[earlystop_cb],
                      verbose=0
                      )
            Y_val_pred = model.predict(self.val_Xs)
            Y_val_pred = np.array([1 if y > 0.5 else 0 for y in Y_val_pred])  # change for genre
            score = f1_score(self.val_Ys, Y_val_pred, average='weighted', pos_label=None)
            result_gs['params'].append(settings)
            result_gs['scores'].append(score)

        print("classes {}".format(self.le.classes_))
        scores = result_gs['scores']
        print("Results")
        for score, param in zip(scores, result_gs['params']):
            print("%0.3f  for %r" % (score, param))
        print()
        print("Best score: {} , param: {}".format(max(scores), result_gs['params'][np.argmax(scores)]))

        self.grid_scores = result_gs

        return result_gs['params'][np.argmax(scores)]

    def create_model(self, settings):
        pass

    def run(self, output_folder, checkpoint_path):
        final_results = {}
        out_file = '{}.txt'.format(self.name)
        with open(os.path.join(output_folder, out_file), 'w') as f:
            with redirect_stdout(f):
                best_settings = self.grid_search()

                # train with best settings

                best_model = self.create_model(best_settings)
                best_model.summary()

                earlystop_cb = callbacks.EarlyStopping(monitor='val_loss', patience=best_settings['patience'],
                                                       verbose=1,
                                                       mode='auto')
                check_cb = callbacks.ModelCheckpoint(
                    checkpoint_path + self.name + '_succes_best_1.hdf5', monitor='val_loss',
                    verbose=1, save_best_only=True, mode='min')

                history = best_model.fit(self.train_Xs, self.train_Ys,
                                         epochs=best_settings['epochs'],
                                         batch_size=best_settings['batch_size'],
                                         validation_data=(self.val_Xs, self.val_Ys),
                                         shuffle=True,
                                         callbacks=[earlystop_cb, check_cb]
                                         )

                ########################################################
                # TEST with trained model
                ########################################################

                Y_test_pred = best_model.predict(self.test_Xs)
                Y_test_pred = np.array([1 if y > 0.5 else 0 for y in Y_test_pred])

                final_results['train_val_dev'] = f1_score(self.test_Ys, Y_test_pred, average='weighted', pos_label=None)

                self.report(self.test_Ys, Y_test_pred)

                print("--" * 25)

                #############################################################
                # Loding the weights checked and predicting with best models
                #############################################################


                best_model.load_weights(checkpoint_path + self.name + '_succes_best_1.hdf5')
                Y_test_pred = best_model.predict(self.test_Xs)
                Y_test_pred = np.array([1 if y > 0.5 else 0 for y in Y_test_pred])
                final_results['checked_model'] = f1_score(self.test_Ys, Y_test_pred, average='weighted', pos_label=None)

                print("Result Loaded Checked model: F1 weighted: {}".format(
                    f1_score(self.test_Ys, Y_test_pred, average='weighted', pos_label=None)))

                #############################################################
                # combine train and dev and train again and test
                #############################################################



                n_epochs = len(history.epoch)
                #fix here
                train_data_keys=list(self.train_Xs.keys())
                X_train = [np.concatenate((self.train_Xs[key], self.val_Xs[key])) for key in train_data_keys]# x, y in zip(self.train_Xs, self.val_Xs)]
                Y_train = np.concatenate((self.train_Ys['success_output'], self.val_Ys['success_output']))

                print("N_epochs: {}".format(n_epochs))
                print("Training on whole data")
                print("Training data shape: {}".format(X_train.shape))

                best_model_all = self.create_model(best_settings)
                best_model_all.fit(X_train, Y_train,
                                   epochs=n_epochs,
                                   batch_size=best_settings['batch_size'],
                                   shuffle=True,
                                   )

                Y_test_pred = best_model_all.predict(self.test_Xs)
                Y_test_pred = np.array([1 if y > 0.5 else 0 for y in Y_test_pred])

                print("<<<Results trained on whole data>>>")
                self.report(self.test_Ys, Y_test_pred)

                print("Result Trained on whole data: F1 weighted: {}".format(
                    f1_score(self.test_Ys, Y_test_pred, average='weighted', pos_label=None)))

                final_results['test_test'] = f1_score(self.test_Ys, Y_test_pred, average='weighted', pos_label=None)

                self.final_results = final_results

                print('Best parameters: {} '.format(best_settings))
                print()
                print("Best F1-weighted Scores {}".format(final_results))

    def print_predictions(self, Y_test, y_pred):
        print('*' * 25)
        print('Actual, Predicted')
        for x, y in zip(Y_test.flatten(), np.array(y_pred).flatten()):
            print("{}, {}".format(x, y))

        print('*' * 25)

    def report(self, Y_test, y_pred):

        print(classification_report(Y_test, y_pred))

        print("")
        print("Confusion matrix")
        print("============================================================")

        print()
        print(confusion_matrix(
            Y_test,
            y_pred))

        test_acc = accuracy_score(Y_test, y_pred) * 100

        p_micro, r_micro, f_micro, _ = precision_recall_fscore_support(Y_test, y_pred, average='micro',
                                                                       pos_label=None)
        p_macro, r_macro, f_macro, _ = precision_recall_fscore_support(Y_test, y_pred, average='macro',
                                                                       pos_label=None)
        p_weighted, r_weighted, f_weighted, _ = precision_recall_fscore_support(Y_test, y_pred, average='weighted',
                                                                                pos_label=None)
        roc_auc = roc_auc_score(y_true=Y_test, y_score=y_pred)

        # Precision recall f1 score for each class : success  and failure
        # For success
        precision_success = precision_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)
        recall_success = recall_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)
        f1_success = f1_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=1)

        # For Failure
        precision_failure = precision_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)
        recall_failure = recall_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)
        f1_failure = f1_score(y_true=Y_test, y_pred=y_pred, average='binary', pos_label=0)

        bac = balanced_accuracy(Y_test, y_pred)

        self.print_predictions(Y_test, y_pred)

        print("")
        print(
            "Accuracy,BAC,ROC_AUC,Precision-Macro,Precision-Micro,Precision-Weighted,Recall-Macro,Recall-Micro,Recall-Weighted,F1-Macro,F1-Micro,F1-Weighted,Failure-Precision,Failure-Recall,Failure-f1,Success-Precision,Success-Recall,Success-F1")
        print(
            "{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}".format(test_acc, bac, roc_auc, p_macro, p_micro,
                                                                           p_weighted, r_macro, r_micro, r_weighted,
                                                                           f_macro, f_micro, f_weighted,
                                                                           precision_failure, recall_failure,
                                                                           f1_failure, precision_success,
                                                                           recall_success, f1_success))


class ExperimentAttention(Experiemnt):
    def create_model(self, settings):
        number_of_genres = 8

        model_inputs = []
        dense_out = []
        for input_name, data in self.train_Xs.items():
            # input layers
            model_inputs.append(Input(shape=(data.shape[-1],), dtype='float32', name=input_name))
            # dense layers
            if input_name == 'genre':
                emb_layer = Embedding(input_dim=number_of_genres, output_dim=settings['genre_emb_size'],
                                      embeddings_initializer=initializers.Orthogonal(gain=1.0, seed=None),name='genre_emb')
                emb_layer_out = emb_layer(model_inputs[-1])
                emb_layer_out = Dropout(settings['dropout'], name='emb_droput')(emb_layer_out)

            else:
                dense = Dense(256, activation='selu', kernel_initializer=settings['initializer'],name='dense_{}'.format(input_name))(model_inputs[-1])
                dense_layer_out = Dropout(settings['dropout'])(dense)
                dense_out.append(dense_layer_out)

        # concat features
        feature_merge = Concatenate(axis=1, name='dense_concat')(dense_out)

        # attention

        genre_attention = AttentionMLP(units=200, activation='tanh')
        genre_attention_out = genre_attention([feature_merge, emb_layer_out])

        # droput

        dropout_2_out = Dropout(settings['dropout'], name='dropout_2')(genre_attention_out)

        # classification


        success_output = Dense(1, activation='sigmoid', name='success_output')(dropout_2_out)

        optimizer = get_optimizers('adam', settings['lr'])

        success_model = Model(inputs=model_inputs, outputs=success_output)
        success_model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'])
        return success_model


class ExperimentBilinear(Experiemnt):
    def create_model(self, settings):

        model_inputs = []
        dense_out = []
        for input_name, data in self.train_Xs.items():
            # input layers
            model_inputs.append(Input(shape=(data.shape[-1],), dtype='float32', name=input_name))
            # dense layers
            dense = Dense(256, activation='selu', kernel_initializer=settings['initializer'],name='dense_{}'.format(input_name))(model_inputs[-1])
            dense_layer_out = Dropout(settings['dropout'])(dense)
            dense_out.append(dense_layer_out)

        # bilinear
        bilinear_layer = BilinearLayer(units=10, activation='selu',name='bilinear')
        bilinear_layer_out = bilinear_layer(dense_out)

        # droput
        dropout_2_out = Dropout(settings['dropout'], name='dropout_2')(bilinear_layer_out)

        # classification
        success_output = Dense(1, activation='sigmoid', name='success_output')(dropout_2_out)

        optimizer = get_optimizers('adam', settings['lr'])

        success_model = Model(inputs=model_inputs, outputs=success_output)
        success_model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'])
        return success_model




class ExperimentBilinearAttention(Experiemnt):
    def create_model(self, settings):
        number_of_genres = 8

        model_inputs = []
        dense_out = []
        for input_name, data in self.train_Xs.items():
            # input layers
            model_inputs.append(Input(shape=(data.shape[-1],), dtype='float32', name=input_name))
            # dense layers
            if input_name == 'genre':
                emb_layer = Embedding(input_dim=number_of_genres, output_dim=settings['genre_emb_size'],
                                      embeddings_initializer=initializers.Orthogonal(gain=1.0, seed=None),name='genre_emb')
                emb_layer_out = emb_layer(model_inputs[-1])
                emb_layer_out = Dropout(settings['dropout'], name='emb_droput')(emb_layer_out)

            else:
                dense = Dense(256, activation='selu', kernel_initializer=settings['initializer'],name='dense_{}'.format(input_name))(model_inputs[-1])
                dense_layer_out = Dropout(settings['dropout'])(dense)
                dense_out.append(dense_layer_out)

        # concat features
        feature_merge = Concatenate(axis=1, name='dense_concat')(dense_out)

        # attention

        genre_attention = AttentionMLP(units=200, activation='tanh')
        genre_attention_out = genre_attention([feature_merge, emb_layer_out])


        #bilinear
        bilinear_layer = BilinearLayer(units=10, activation='selu', name='bilinear')
        bilinear_layer_out = bilinear_layer(dense_out)

        # droput

        dropout_2 = Dropout(settings['dropout'], name='dropout_2')(genre_attention_out)
        dropout_2_attention = dropout_2(genre_attention_out)
        dropout_2_bilinear = dropout_2(bilinear_layer_out)

        #concat

        attention_genre_merge = Concatenate(axis=-1, name='attention_bilinear_concat')([dropout_2_attention,dropout_2_bilinear])

        # classification


        success_output = Dense(1, activation='sigmoid', name='success_output')(attention_genre_merge)

        optimizer = get_optimizers('adam', settings['lr'])

        success_model = Model(inputs=model_inputs, outputs=success_output)
        success_model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'])
        return success_model


class ExperimentConcatST(Experiemnt):
    def create_model(self, settings):

        model_inputs = []
        dense_out = []
        for input_name, data in self.train_Xs.items():
            # input layers
            model_inputs.append(Input(shape=(data.shape[-1],), dtype='float32', name=input_name))
            # dense layers
            dense = Dense(256, activation='selu', kernel_initializer=settings['initializer'],name='dense_{}'.format(input_name))(model_inputs[-1])
            dense_layer_out = Dropout(settings['dropout'])(dense)
            dense_out.append(dense_layer_out)


        # concat features
        feature_merge = Concatenate(axis=-1, name='dense_concat')(dense_out)

        # droput
        dropout_2_out = Dropout(settings['dropout'], name='dropout_2')(feature_merge)

        #dense
        dense_final=Dense(200,activation='selu', kernel_initializer=settings['initializer'],name='dense_final')(dropout_2_out)

        # droput
        dropout_3_out = Dropout(settings['dropout'], name='dropout_3')(dense_final)

        # classification
        success_output = Dense(1, activation='sigmoid', name='success_output')(dropout_3_out)

        optimizer = get_optimizers('adam', settings['lr'])

        success_model = Model(inputs=model_inputs, outputs=success_output)
        success_model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'])
        return success_model

