# -*- coding: utf-8 -*-
from __future__ import division, print_function
import matplotlib.pyplot as plt
import numpy as np
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import f1_score
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC

from manage import app
import os


def plot_learning_curve(title, train_sizes, train_scores, test_scores):

    print (train_scores)
    print (test_scores)
    print (train_sizes)


    plt.figure()
    plt.title(title)
    plt.xlabel("Number of Sentences")
    plt.ylabel("Score")

    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)

    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    plt.grid()

    #plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
    #                 train_scores_mean + train_scores_std, alpha=0.1,
    #                 color="r")

    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")

    #plt.plot(train_sizes, train_scores_mean, '<-', color="r",
    #         label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Cross-validation score")

    plt.legend(loc="best")

    plt.savefig(os.path.join(app.CHART_DIR, title + '.png'))
    return plt



def compute_score(X_test, Y_test, model):
    def transform_labels(Y):
        return np.array([ int(label[-1]) for label in Y])


    Y_fit = model.predict(X_test)

    Y_test_tranform=transform_labels(Y_test)
    Y_fit_tranform=transform_labels(Y_fit)

    return f1_score(y_true=Y_test_tranform, y_pred=Y_fit_tranform, average='weighted', pos_label=None)


def train_test_errors(X,Y, size, model):
    train_scores, test_scores = [], []
    counter = 0
    skf = StratifiedKFold(Y,n_folds=3)
    for train, test in skf:
        counter += 1
        X_train, y_train = [book.of_size(size) for book in X[train]], [y for y in Y[train]]
        X_test, y_test = [book.of_size(1000) for book in X[test]], [y for y in Y[test]]
        # print X_train,y_train
        # print (y_train[0])
        model.fit(X_train, y_train)

        test_scores.append(compute_score(X_test, y_test, model))  # error

        train_scores.append(compute_score(X_train, y_train, model))

        print ("Fold %d done" % counter)
    return train_scores, test_scores


def run_learning_curve(X,Y,name):
    from features import  get_feature
    train_sizes = np.append(np.linspace(5, 100, 10).astype(int), np.linspace(200, 1000, 9).astype(int))
    test_scores = []
    train_scores = []

    model =Pipeline([('char_3',get_feature('char_tri')),('svm',LinearSVC(random_state=1234,C=10.0))])

    for i, size in enumerate(train_sizes):
        train_errors, test_errors = train_test_errors(X, Y,size, model)
        train_scores.append(train_errors)
        test_scores.append(test_errors)


        print( "Done {} ".format(i))

    plot_learning_curve(name, train_sizes, np.array(train_scores), np.array(test_scores))
