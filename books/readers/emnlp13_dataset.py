# -*- coding: utf-8 -*-
from __future__ import print_function
import os
from  .book import Book


class EMNLP13Reader(object):
    def __init__(self, dirname, genres=None):
        self.dirname = dirname
        if genres is not None:
            self.genres = genres
        else:
            self.genres = sorted(
                ["Adventure_Stories", "Detective_Mystery", "Fiction", "Historical_Fiction", "Short_Stories",
                 "Love_Stories", "Science_Fiction", "Poetry"])

    def __iter__(self):
        for genre in self.genres:
            for fold in os.listdir(os.path.join(self.dirname, genre)):
                if fold == '.DS_Store':
                    continue
                for category in ['failure' + fold[-1], 'success' + fold[-1]]:
                    for fid in os.listdir(os.path.join(self.dirname, genre, fold, category)):
                        fname = os.path.join(self.dirname, genre, fold, category, fid)
                        if fid.startswith('.DS_Store') or not os.path.isfile(fname):
                            continue
                        if category.startswith("failure"):
                            success = 0
                        else:
                            success = 1
                        sentic_file = os.path.join(self.dirname, 'sentic_concepts', genre,
                                                   "{}_{}_st_parser.txt.json".format(genre, fid.replace('.txt',
                                                                                                        '')))  # Adventure_Stories_9968_st_parser.txt.json
                        if not os.path.exists(sentic_file):
                            raise OSError("Sentic file does not exit: {}".format(sentic_file))

                        stanford_parse_file = os.path.join(self.dirname, genre, fold, category + '_parser',
                                                           fid.replace('.txt',
                                                                       '_st_parser.txt'))

                        if not os.path.exists(stanford_parse_file):
                            raise OSError("Stanford parse file does not exit: {}".format(stanford_parse_file))

                        yield Book(book_path=fname, book_id="{}_{}".format(genre,fid), genre=genre, success=success, avg_rating=0.0,
                                   sentic_file=sentic_file, stanford_parse_file=stanford_parse_file)
