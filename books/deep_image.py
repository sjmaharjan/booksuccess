from keras.preprocessing.image import ImageDataGenerator
from keras.applications.resnet50 import ResNet50
from keras.applications.vgg16 import VGG16
from keras.layers import GlobalAveragePooling2D, Dense
from deep_utils import get_optimizers
from keras.preprocessing import image
from keras.models import Model
import sys

def build_deep_model(settings, n_classes, base_image_model='vgg'):
    if base_image_model == 'vgg':
        base_model = VGG16(weights='imagenet', include_top=False)
    elif base_image_model == 'resnet':
        base_model = ResNet50(weights='imagenet', include_top=False)
    else:
        raise ValueError("Model not defined")

    # add a global spatial average pooling layer
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    # let's add a fully-connected layer
    x = Dense(1024, activation='selu')(x)
    # and a logistic layer --
    predictions = Dense(n_classes, activation=settings['final_activation'])(x)

    # this is the model we will train
    model = Model(inputs=base_model.input, outputs=predictions)

    # first: train only the top layers (which were randomly initialized)
    # i.e. freeze all convolutional InceptionV3 layers
    for layer in base_model.layers:
        layer.trainable = False

    # compile the model (should be done *after* setting layers to non-trainable)
    model.compile(optimizer=settings['optimizer'], loss=settings['loss'])

    return model


def run_vgg_exp(settings):
    model = build_deep_model(settings)
    model.fit_generator(...)
    print("Parameters: {}".format(settings))


if __name__ == '__main__':
    exp = sys.argv[1:]
    if exp == 'genre':
        n_classes = 8
    elif exp == 'success':
        n_classes = 2

    settings = {
        'batch_size': 16,
        'epochs': 100,
        'final_activation': 'softmax' if n_classes > 2 else 'sigmoid',
        'loss': 'categorical_crossentropy' if n_classes > 2 else 'binary_crossentropy',
        'optimizer': get_optimizers('rmsprop')

    }

    root = ''

    # this is the augmentation configuration we will use for training
    train_datagen = ImageDataGenerator(
        rescale=1. / 255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)

    # this is the augmentation configuration we will use for testing:
    # only rescaling
    test_datagen = ImageDataGenerator(rescale=1. / 255)

    # this is a generator that will read pictures found in
    # subfolers of 'data/train', and indefinitely generate
    # batches of augmented image data
    train_generator = train_datagen.flow_from_directory(
        root + 'train',  # this is the target directory
        target_size=(224, 224),  # all images will be resized to 150x150
        batch_size=settings['batch_size'],
        class_mode='binary')  # since we use binary_crossentropy loss, we need binary labels

    # this is a similar generator, for validation data
    validation_generator = test_datagen.flow_from_directory(
        root + 'validation',
        target_size=(224, 224),
        batch_size=settings['batch_size'],
        class_mode='binary')



