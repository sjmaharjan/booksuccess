from __future__ import print_function
import pandas as pd
import os
from urllib.request import urlopen,urlretrieve

import time

from bs4 import BeautifulSoup
import csv


def books_to_download(meta_exl_file, genres):
    def filter_books(df, genre):
        books = set()
        if genre in ['Fiction', 'Science fiction', 'Detective and mystery stories']:
            books_df = df[df['pg.SUBJECT'] == genre]
        else:
            books_df = df[
                df['pg.SUBJECT'].str.lower().str.contains(genre.lower()) == True]
        print ("Total Books in genre {g}: {total}".format(g=genre, total=len(books_df)))
        for index, row in books_df.iterrows():
            # print (row['gr.URL'])
            books.add((row['gr.URL'], row['FILENAME'].replace('.txt', '.html')))

        return books

    df = pd.read_excel(meta_exl_file, skiprows=[0], index_col=0)
    df_with_reviews = df[df['gr.AVERAGE_RATING'] > 0.0]
    df_wo_reviews = df[df['gr.AVERAGE_RATING'] == 0.0]

    book_lst = set()
    for genre in genres:
        book_lst |= filter_books(df_with_reviews, genre)
        book_lst |= filter_books(df_wo_reviews, genre)

    return book_lst


def download_from_link(link):
    page = urlopen(link)
    return page.read().decode('utf-8')


def download_goodreads(meta_exl_file, genres, output_dir, book_meta_file):
    print (genres)
    books = books_to_download(meta_exl_file, genres)

    print ("Total books to download: {}".format(len(books)))

    files_downloaded = os.listdir(output_dir)
    for download_link, filename in books:
        if filename in files_downloaded:
            continue
        print (filename)
        page_content = download_from_link(download_link)
        with open(os.path.join(output_dir, filename), 'w', encoding='utf-8') as fid:
            fid.write(page_content)
        time.sleep(7)

    print ("Download complete ..")
    print ("Starting parsing the download files for collecting book meta data.")
    save_book_meta(book_meta_file, output_dir)

    print ("All process done.")


def cover_image(book):
    soup = BeautifulSoup(book, "lxml")
    book_cover = soup.find("img", {"id":"coverImage"})
    img_url=book_cover['src']
    print ("Downloading image"+img_url)
    return img_url

def reviews_rating_stars(book):
    soup = BeautifulSoup(book, "lxml")
    book_meta = soup.find("div", {"id": "bookMeta"})
    if book_meta:
        avg_stars = float(book_meta.find("span", {"class": "average", "itemprop": "ratingValue"}).text)
        ratings_count = int(book_meta.find("span", {"class": "value-title", "itemprop": "ratingCount"})["title"])
        reviews_count = int(book_meta.find("span", {"class": "count"}).find("span", {"class": "value-title"})["title"])
        return avg_stars, ratings_count, reviews_count
    else:
        return None


def update_old_link(fname):
    book_title = fname.split("_")[-1].split(".")[0]
    download_link = "http://www.goodreads.com/search?search[from_nav]=true&query=%s" % book_title
    page_content = download_from_link(download_link)
    soup = BeautifulSoup(page_content, "lxml")
    updated_link = \
        soup.find("table", {"class": "tableList"}).find("tr").find("a", {"class", "bookTitle"})["href"].split("?")[0]
    return "http://www.goodreads.com%s" % updated_link


def save_book_meta(output_file, data_path):
    not_found = 0
    with open(output_file, 'w', encoding='utf-8') as out:
        fieldnames = ['FILENAME', "AVG_RATING_2016", "RATINGS_2016", "REVIEWS_2016"]
        csv_writer = csv.DictWriter(out, fieldnames=fieldnames, delimiter='\t')
        csv_writer.writeheader()
        for fname in os.listdir(data_path):
            if fname.endswith('.html'):
                with open(os.path.join(data_path, fname), "r", encoding='utf-8') as fhandle:
                    try:
                        stats = reviews_rating_stars(fhandle.read())
                        if stats:
                            csv_writer.writerow(
                                {"FILENAME": fname.replace('.html', '.txt'), "AVG_RATING_2016": stats[0],
                                 "RATINGS_2016": stats[1], "REVIEWS_2016": stats[2]})
                        else:
                            print (fname, end=' ')
                            updated_link = update_old_link(fname)
                            print(updated_link)
                            page_content = download_from_link(updated_link)
                            with open(os.path.join(output_dir, fname), 'w', encoding='utf-8') as fid:
                                fid.write(page_content)
                            stats = reviews_rating_stars(page_content)
                            print(stats)
                            csv_writer.writerow(
                                {"FILENAME": fname.replace('.html', '.txt'), "AVG_RATING_2016": stats[0],
                                 "RATINGS_2016": stats[1], "REVIEWS_2016": stats[2]})
                            time.sleep(6)
                            if not stats:
                                not_found += 1
                    except Exception as e:
                        print(fname)
    print ("Total files not found :{}".format(not_found))



def save_book_covers(output_directory, data_path):
    not_found = 0
    for fname in os.listdir(data_path):
        if fname.endswith('.html'):
            with open(os.path.join(data_path, fname), "r", encoding='utf-8') as fhandle:
                try:
                    img_url = cover_image(fhandle.read())
                    if img_url:
                       urlretrieve(img_url, os.path.join(output_directory,fname.replace('.html', '.jpg')))
                    else:
                        print(fname, end=' ')
                        updated_link = update_old_link(fname)
                        print(updated_link)
                        page_content = download_from_link(updated_link)
                        with open(os.path.join(output_directory, fname), 'w', encoding='utf-8') as fid:
                            fid.write(page_content)
                        img_url = cover_image(page_content)
                        urlretrieve(img_url, os.path.join(output_directory,fname.replace('.html', '.jpg')))
                        #print(img_url)

                        time.sleep(6)
                        if not img_url:
                            not_found += 1
                except Exception as e:
                    print("Error",fname)
    print("Total files not found :{}".format(not_found))


if __name__ == '__main__':
    # save_book_meta(output_file=config['goodreads']['book_metadata'], data_path=config['goodreads']['output_directory'])

    # download_goodreads(meta_exl_file=config['goodreads']['meta_file'], genres=config['goodreads']['genres'],
    #                    output_dir=config['goodreads']['output_directory'],
    #                    book_meta_file=config['goodreads']['book_metadata'])

    output_dir='/home/sjmaharjan/Books/booksuccess/data/goodreads_cover'
    data_dir='/home/sjmaharjan/Books/booksuccess/data/goodreads'
    save_book_covers(output_dir, data_dir)
