# -*- coding: utf-8 -*-
from __future__ import division, print_function
import numpy as np
from scipy.stats import entropy
from minepy import MINE
from statsmodels.sandbox.stats.runs import mcnemar

# REF: Building machine learning system with Python



def mutual_information(x, y, bins=10):
    counts_xy, bins_x, bins_y = np.histogram2d(x, y, bins=(bins, bins))
    counts_x, bins = np.histogram(x, bins=bins)
    counts_y, bins = np.histogram(y, bins=bins)

    counts_xy += 1
    counts_x += 1
    counts_y += 1
    P_xy = counts_xy / np.sum(counts_xy, dtype=float)
    P_x = counts_x / np.sum(counts_x, dtype=float)
    P_y = counts_y / np.sum(counts_y, dtype=float)

    I_xy = np.sum(P_xy * np.log2(P_xy / (P_x.reshape(-1, 1) * P_y)))

    return I_xy / (entropy(counts_x) + entropy(counts_y))



def maximal_information_coefficient(x,y):
    """
    computes maximal inforamtion coefficient between two varaibles
    :param x: np array
    :param y: np array
    :return: mic score
    """
    mine = MINE(alpha=0.6, c=15, est="mic_approx")
    mine.compute_score(x, y)
    return  mine.mic()



def mcnemar_test(Y1,Y2):
    return mcnemar(Y1,Y2,exact=False)

