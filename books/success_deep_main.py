from sklearn import preprocessing

from dataset import Dataset
from experiments.success_deep_exp import ExperimentAttention

if __name__ == '__main__':
    checkpoint_path = '/uhpc/solorio/suraj/Books/results/cover_success/checkpoints/'
    SUCCESS_OUTPUT = '/uhpc/solorio/suraj/Books/results/cover_success/'

    features = ['vgg', 'book10002vec_dmm']

    # settings  = {
    #     'batch_size': [8,1],
    #     'epochs': [100],
    #     'initializer': ['glorot_uniform'],
    #     'dropout': [0.5,0.3],
    #     'lr':[0.00001,0.0001,0.001],
    #     'patience':[10,15,20]
    #
    # }

    settings = {
        'batch_size': [8],
        'epochs': [100],
        'initializer': ['glorot_uniform'],
        'dropout': [0.5],
        'lr': [0.00001],
        'patience': [10],
        'genre_emb_size':[100]

    }

    dataset = Dataset(features=features, label_extractor='success')

    X_train, Y_train = dataset.get_training_data()
    X_val, Y_val = dataset.get_validaiontion_data()
    X_test, Y_test = dataset.get_test_data()


    for feature in features:
        print("Traning shape {}:{}".format(feature,X_train[feature].shape))
        print("Val shape {}:{}".format(feature,X_val[feature].shape))
        print("Test shape {}:{}".format(feature,X_test[feature].shape))

        print()

    le = preprocessing.LabelEncoder()

    Y_train['success_output'] = le.fit_transform(Y_train['success_output'])
    Y_val['success_output'] = le.transform(Y_val['success_output'])
    Y_test['success_output'] = le.transform(Y_test['success_output'])

    experiment = ExperimentAttention(train_Xs=X_train, train_Ys=Y_train, val_Xs=X_val, val_Ys=Y_val, test_Xs=X_test,
                                     test_Y=Y_test, le=le, params=settings, name='attention_multimodal')

    experiment.run(output_folder=SUCCESS_OUTPUT, checkpoint_path=checkpoint_path)



