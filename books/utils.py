# -*- coding: utf-8 -*-
from __future__ import print_function
from manage import basedir
import os
import shutil


def read_yaml(fpath=None):
    import yaml
    if fpath:
        split_file = os.path.join(fpath)
    else:
        split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    with open(split_file, 'r') as stream:
        try:
            data = yaml.load(stream)
            return data

        except yaml.YAMLError as exc:
            print(exc)
            raise OSError("Error reading the split file")


def move_images(image_path, output_dir):
    def create_directory(path):
        os.mkdir(path)

    data = read_yaml()
    for fold, book_names in data.items():
        for book_name in book_names:
            if fold == 'test':
                if not os.path.exists(output_dir + '/test'):
                    create_directory(output_dir + '/test')
                try:
                    shutil.copyfile(image_path + '/' + book_name.replace('.txt', '.jpg').replace('+', '-'),
                                    output_dir + '/test/' + book_name.replace('.txt', '.jpg').replace('+', '-'))
                except Exception as e:
                    print(book_name)
            else:
                if not os.path.exists(output_dir + '/train'):
                    create_directory(output_dir + '/train')
                try:
                    shutil.copyfile(image_path + '/' + book_name.replace('.txt', '.jpg').replace('+', '-'),
                                    output_dir + '/train/' + book_name.replace('.txt', '.jpg').replace('+', '-'))
                except Exception as e:
                    print(book_name)


def prepare_images(image_path, info_file, output_dir):
    import pickle
    def create_directory(path):
        os.makedirs(path)

    data = pickle.load(open(info_file,'rb'))
    for split, labels in data.items():
        if not os.path.exists(output_dir + '/{}'.format(split)):
            create_directory(output_dir + '/{}'.format(split))
        for l, img_file in labels.items():
            if not os.path.exists(output_dir + '/{}/{}'.format(split, l)):
                create_directory(output_dir + '/{}/{}'.format(split, l))
                for book_name in img_file:
                    try:
                        if os.path.exists(image_path + '/' + book_name.replace('.txt', '.jpg').replace('+', '-')):
                            img_to_copy= book_name.replace('.txt', '.jpg').replace('+', '-')
                        elif os.path.exists(image_path + '/' + book_name.replace('.txt', '.jpeg').replace('+', '-')):
                            img_to_copy= book_name.replace('.txt', '.jpeg').replace('+', '-')
                        elif os.path.exists(image_path + '/' + book_name.replace('.txt', '.png').replace('+', '-')):
                            img_to_copy= book_name.replace('.txt', '.png').replace('+', '-')

                        shutil.copyfile(image_path + '/' +img_to_copy,
                                        output_dir + '/{}/{}/'.format(split, l) + img_to_copy)
                    except Exception as e:
                        print(book_name)
