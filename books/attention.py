from __future__ import division, print_function
from keras import backend as K
from keras.engine.topology import Layer
from keras.layers import Dense, activations, initializers


class AttentionMV(Layer):
    """
    Keras layer to compute an attention vector on an incoming matrix
    and a user provided context vector.

    # Input
        enc - 3D Tensor of shape (BATCH_SIZE, MAX_TIMESTEPS, EMBED_SIZE)
        ctx - 2D Tensor of shape (BATCH_SIZE, EMBED_SIZE) (optional)

    # Output
        2D Tensor of shape (BATCH_SIZE, EMBED_SIZE)
    # Usage
        enc = Bidirectional(GRU(EMBED_SIZE,return_sequences=True))(...)
        # with user supplied vector
        ctx = GlobalAveragePooling1D()(enc)
        att = AttentionMV()([enc, ctx])

    """

    def __init__(self,
        units,
        activation = None,
        use_bias = True,
        kernel_initializer = 'glorot_uniform',
        bias_initializer = 'zeros',
        ** kwargs):
        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        self.units = units
        self.activation = activations.get(activation)
        self.use_bias = use_bias
        self.kernel_initializer = initializers.get(kernel_initializer)
        self.bias_initializer = initializers.get(bias_initializer)
        self.supports_masking = True

        super(AttentionMV, self).__init__(**kwargs)

    def build(self, input_shape):
        assert type(input_shape) is list and len(input_shape) == 2
        # W: (EMBED_SIZE, units)
        # Wg:(GENRE_EMB_SIZE, units)
        # b: (units)
        # v: (units)
        self.W = self.add_weight(name="W_{:s}".format(self.name),
                                 shape=(input_shape[0][-1], self.units),
                                 initializer="normal")
        self.Wg = self.add_weight(name="W_g{:s}".format(self.name),
                                 shape=(input_shape[1][-1], self.units),
                                 initializer=self.kernel_initializer)

        self.b = self.add_weight(name="b_{:s}".format(self.name),
                                 shape=(1, self.units),
                                 initializer=self.bias_initializer)

        self.v = self.add_weight(name="v_{:s}".format(self.name),
                                 # shape=(input_shape[0][1],self.units),
                                 shape=(self.units,1),
                                 initializer="normal")
        super(AttentionMV, self).build(input_shape)

    def call(self, xs, mask=None):
        # input: [x, u]
        # x: (BATCH_SIZE, MAX_TIMESTEPS, EMBED_SIZE)
        # g: (BATCH_SIZE, GENRE_EMB_SIZE)
        x, g = xs
        # et: (BATCH_SIZE, MAX_TIMESTEPS)



        input_mlp=K.dot(x, self.W)
        atteng=K.dot(g, self.Wg)
        atteng_r=K.expand_dims(atteng,axis=1)
        print(input_mlp.eval())
        print(input_mlp.shape)
        print(atteng_r.shape)
        b1= K.expand_dims(self.b,axis=0)
        print("b1:", b1.shape)
        # et1 =self.activation(atteng_r + input_mlp + self.b)# + )
        et1 =self.activation(atteng_r + input_mlp + b1)# + )
        print("et1", et1.shape)
        print("v: ", self.v.shape)
        et= K.dot(et1, self.v)
        # at: (BATCH_SIZE, MAX_TIMESTEPS)

        print("et:", et.shape)
        et=K.squeeze(et,axis=-1)
        print("et:", et.shape)
        at = K.softmax(et)
        if mask is not None and mask[0] is not None:
            at *= K.cast(mask, K.floatx())
        # ot: (BATCH_SIZE, MAX_TIMESTEPS, EMBED_SIZE)
        atx = K.expand_dims(at, axis=-1)
        ot = atx * x
        # output: (BATCH_SIZE, EMBED_SIZE)
        return K.sum(ot, axis=1)

    def compute_mask(self, input, input_mask=None):
        # do not pass the mask to the next layers
        return None

    def compute_output_shape(self, input_shape):
        # output shape: (BATCH_SIZE, EMBED_SIZE)
        return (input_shape[0][0], input_shape[0][-1])

    def get_config(self):
        return super(AttentionMV, self).get_config()


if __name__ == '__main__':
   import numpy as np
   layer_attention= AttentionMV(units=6,activation='tanh')
   lstm_output=K.variable(np.array([[[1,2,1,2],[2,3,3,5],[4,5,5,7]],
                         [[2,3,6,9],[4,5,2,7],[5,6,7,9]]]))

   genre=K.variable(np.array([[1, 0, 1,2,3], [1,0,5,7,7]]))

   layer_attention([lstm_output,genre])

