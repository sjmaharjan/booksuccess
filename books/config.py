import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    STANFORD_PARSER = '/home/ritual/Books/resources/stanford-parser-full-2015-12-09/lexparser.sh'
    CHART_DIR=os.path.join(basedir, '../charts')

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True

    STANFORD_PARSER_OUTPUT = '/home/suraj/resouces/build_may_05_2016/data_parse'  # '/home/booxby/book_st' #'/home/suraj/resouces/build_may_05_2016/data_parse'
    SENTIC_PARSE_OUTPUT = '/home/suraj/resouces/build_may_05_2016/sentic_parse'  # '/home/booxby/book_senti'#''/home/suraj/resouces/build_may_05_2016/sentic_parse'

    FEATURES = [
        'alex_net',
        'alex_net_norm',
        # 'vgg16',
        # 'vgg16_norm',
        # 'resnet50',
        # 'resnet50_norm',


        #best_features + image features
        # ['book10002vec_dmm','vgg16'],
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'categorical_char_ngram_mid_word','vgg16'],
        # ['writing_density_scaled', 'book10002vec_dmm', 'rnn_f1_weighted','vgg16'],
        # ['book10002vec_dbow_dmm','vgg16'],
        # ['rnn_f1_weighted','vgg16'],
        # ['clausal','vgg16'],
        # ['readability_scaled','vgg16'],
        # ['bigram','vgg16'],
        #
        # ['book10002vec_dmm','vgg16_norm'],
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'categorical_char_ngram_mid_word','vgg16_norm'],
        # ['writing_density_scaled', 'book10002vec_dmm', 'rnn_f1_weighted','vgg16_norm'],
        # ['book10002vec_dbow_dmm','vgg16_norm'],
        # ['rnn_f1_weighted','vgg16_norm'],
        # ['clausal','vgg16_norm'],
        # ['readability_scaled','vgg16_norm'],
        # ['bigram','vgg16_norm'],
        #
        # ['book10002vec_dmm','resnet50'],
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'categorical_char_ngram_mid_word','resnet50'],
        # ['writing_density_scaled', 'book10002vec_dmm', 'rnn_f1_weighted','resnet50'],
        # ['book10002vec_dbow_dmm','resnet50'],
        # ['rnn_f1_weighted','resnet50'],
        # ['clausal','resnet50'],
        # ['readability_scaled','resnet50'],
        # ['bigram','resnet50'],
        #
        # ['book10002vec_dmm','resnet50_norm'],
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'categorical_char_ngram_mid_word','resnet50_norm'],
        # ['writing_density_scaled', 'book10002vec_dmm', 'rnn_f1_weighted','resnet50_norm'],
        # ['book10002vec_dbow_dmm','resnet50_norm'],
        # ['rnn_f1_weighted','resnet50_norm'],
        # ['clausal','resnet50_norm'],
        # ['readability_scaled','resnet50_norm'],
        # ['bigram','resnet50_norm'],


    #     'trigram',
    #     ['trigram', 'vgg16'],
    #     ['trigram', 'vgg16_norm'],
    #     ['trigram', 'resnet50'],
    #     ['trigram','resnet50_norm'],
    #
    #
    # ['char_4_gram', 'vgg16'],
    #     ['char_4_gram', 'vgg16_norm'],
    #     ['char_4_gram', 'resnet50'],
    #     ['char_4_gram', 'resnet50_norm'],
    #
    #     ['char_5_gram', 'vgg16'],
    #     ['char_5_gram', 'vgg16_norm'],
    #     ['char_5_gram', 'resnet50'],
    #     ['char_5_gram', 'resnet50_norm'],




        # 'resnet50',
        # 'resnet50_norm'
        # ['unigram','vgg16'],
        # ['unigram','vgg16_norm'],
        # ['unigram','resnet50'],
        # ['unigram','resnet50_norm'],
        #
        # ['bigram', 'vgg16'],
        # ['bigram', 'vgg16_norm'],
        # ['bigram', 'resnet50'],
        # ['bigram', 'resnet50_norm'],
        #
        # ['char_tri', 'vgg16'],
        # ['char_tri', 'vgg16_norm'],
        # ['char_tri', 'resnet50'],
        # ['char_tri', 'resnet50_norm'],


        # # Lexical Features
        # word ngrams
        # 'unigram',
        # 'bigram',
        # 'trigram',
        # #
        # # ['unigram','rnn'],
        # # ['bigram','rnn'],
        # # ['trigram','rnn'],
        # # # # # #
        # # #
        # # ['unigram', 'bigram'],
        # # ['bigram', 'trigram'],
        # # ['unigram', 'bigram', 'trigram'],
        # # # #
        # # # # #  # char ngrams
        # 'char_tri',
        # 'char_4_gram',
        # 'char_5_gram',
        # ['char_tri', 'char_4_gram', 'char_5_gram'],
        # # # #
        # # # # #  # Typed char ngrams
        # 'categorical_char_ngram_beg_punct',
        # 'categorical_char_ngram_mid_punct',
        # 'categorical_char_ngram_end_punct',
        # 'categorical_char_ngram_multi_word',
        # 'categorical_char_ngram_whole_word',
        # 'categorical_char_ngram_mid_word',
        # 'categorical_char_ngram_space_prefix',
        # 'categorical_char_ngram_space_suffix',
        # 'categorical_char_ngram_prefix',
        # 'categorical_char_ngram_suffix',
        # # # # # #
        # # # # # # #Syntactic Features
        # # 'pos',
        # 'phrasal',
        # 'clausal',
        # 'phr_cls',
        # 'lexicalized',
        # 'unlexicalized',
        # 'gp_lexicalized',
        #'gp_unlexicalized',
        # #  #
        # # # # #  # WR and Readability
        #'writing_density',
        # 'writing_density_scaled',
        # # #
        #'readability',
        # 'readability_scaled',
        # # #
        # # # # # # #Phonetic Features
        # 'phonetic',
        # 'phonetic_scores',
        # ['phonetic_scores', 'phonetic'],
        # #
        # #
        # #
        # # # # # sentic concepts and scores
        #'concepts_score',
        #'concepts',
        # ['concepts_score', 'concepts','rnn'],
        # # #
        # # # # # # word embeddings
        # 'google_word_emb',
        # 'gutenberg_word_emb',
        # 'gutenberg_cbow_word_emb',
        # #
        # # # # # book vectors
        # 'book2vec_dmc',
        # 'book2vec_dbow',
        # 'book2vec_dmm',
        # 'book2vec_dbow_dmm',
        # 'book2vec_dbow_dmc',
        # #
        # 'book10002vec_dmc',
        # 'book10002vec_dbow',
        # 'book10002vec_dmm',
        # 'book10002vec_dbow_dmm',
        # 'book10002vec_dbow_dmc',
        # #
        # # # # # sentiments
         #'swn',
        # 'swn_batch',
        # #
        # # # # # skip grams
        #
        # #
        # # # # # stress
        # 'stress_ngrams',
        # 'stress_scores',
        # ['stress_ngrams', 'stress_scores'],
        # #
        # 'two_skip_2_grams',
        # 'two_skip_3_grams',
        #
        #
        #
        #
        # ['concepts_score', 'concepts', 'book10002vec_dmm'],
        # ['concepts_score', 'concepts', 'book10002vec_dbow_dmm'],
        # ['concepts_score', 'concepts', 'book2vec_dbow_dmm'],
        #
        # ['writing_density_scaled', 'book10002vec_dmm'],
        # ['writing_density_scaled', 'book10002vec_dbow_dmm'],
        # ['writing_density_scaled', 'book2vec_dbow_dmm'],
        #
        # ['readability_scaled', 'book10002vec_dmm'],
        # ['readability_scaled', 'book10002vec_dbow_dmm'],
        # ['readability_scaled', 'book2vec_dbow_dmm'],
        #
        # ['concepts_score', 'concepts', 'writing_density_scaled'],
        # ['concepts_score', 'concepts', 'readability_scaled'],
        #
        #
        # # ['writing_density_scaled', 'book10002vec_dmm', 'rnn'],
        # ['concepts_score', 'concepts', 'readability_scaled','book10002vec_dmm'],
        # ['concepts_score', 'concepts', 'readability_scaled','book10002vec_dbow_dmm'],
        # ['concepts_score', 'concepts', 'writing_density_scaled','book10002vec_dmm'],
        # ['concepts_score', 'concepts', 'writing_density_scaled','book10002vec_dbow_dmm'],
        #
        #
        #
        #
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'categorical_char_ngram_whole_word'],
        # ['concepts_score', 'concepts', 'readability_scaled', 'categorical_char_ngram_whole_word'],
        # #
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'categorical_char_ngram_mid_word'],
        # ['concepts_score', 'concepts', 'readability_scaled', 'categorical_char_ngram_mid_word'],
        # # #
        # ['categorical_char_ngram_beg_punct',
        #  'categorical_char_ngram_mid_punct',
        #  'categorical_char_ngram_end_punct',
        #  'categorical_char_ngram_multi_word',
        #  'categorical_char_ngram_whole_word',
        #  'categorical_char_ngram_mid_word',
        #  'categorical_char_ngram_space_prefix',
        #  'categorical_char_ngram_space_suffix',
        #  'categorical_char_ngram_prefix',
        #  'categorical_char_ngram_suffix'],
        # #
        # [
        #     'categorical_char_ngram_prefix', 'categorical_char_ngram_space_prefix',
        #     'categorical_char_ngram_space_suffix', 'categorical_char_ngram_whole_word',
        #     'categorical_char_ngram_mid_word', 'categorical_char_ngram_beg_punct', 'categorical_char_ngram_mid_punct'
        # ],
        # # #
        # [
        #     'unigram', 'bigram', 'trigram', 'char_5_gram', 'categorical_char_ngram_whole_word',
        #     'two_skip_2_grams', 'lexicalized', 'google_word_emb', 'book10002vec_dbow_dmm', 'swn',
        #     'concepts_score', 'concepts'
        # ],
        # [
        #     'unigram', 'bigram', 'trigram', 'char_5_gram', 'categorical_char_ngram_whole_word',
        #     'two_skip_2_grams', 'lexicalized', 'swn',
        #     'concepts_score', 'concepts'
        # ],
        # # #




        #rnn combinations
        #
        #

        #
        # ['unigram','rnn_f1_weighted'],
        # ['bigram','rnn_f1_weighted'],
        # ['trigram','rnn_f1_weighted'],
        # # # # # # #
        # # #
        # ['unigram', 'bigram','rnn_f1_weighted'],
        # ['bigram', 'trigram','rnn_f1_weighted'],
        # ['unigram', 'bigram', 'trigram','rnn_f1_weighted'],
        # # # #
        # # # # #  # char ngrams
        # ['char_tri','rnn_f1_weighted'],
        # ['char_4_gram','rnn_f1_weighted'],
        # ['char_5_gram','rnn_f1_weighted'],
        # ['char_tri', 'char_4_gram', 'char_5_gram','rnn_f1_weighted'],
        # # # #
        # # # # #  # Typed char ngrams
        # ['categorical_char_ngram_beg_punct','rnn_f1_weighted'],
        # ['categorical_char_ngram_mid_punct','rnn_f1_weighted'],
        # ['categorical_char_ngram_end_punct','rnn_f1_weighted'],
        # ['categorical_char_ngram_multi_word','rnn_f1_weighted'],
        # ['categorical_char_ngram_whole_word','rnn_f1_weighted'],
        # ['categorical_char_ngram_mid_word','rnn_f1_weighted'],
        # ['categorical_char_ngram_space_prefix','rnn_f1_weighted'],
        # ['categorical_char_ngram_space_suffix','rnn_f1_weighted'],
        # ['categorical_char_ngram_prefix','rnn_f1_weighted'],
        # ['categorical_char_ngram_suffix','rnn_f1_weighted'],
        # # # # # #
        # # # # # # #Syntactic Features
        # # ['pos','rnn_f1_weighted'],
        # # ['phrasal','rnn_f1_weighted'],
        # ['clausal','rnn_f1_weighted'],
        # ['phr_cls','rnn_f1_weighted'],
        # ['lexicalized','rnn_f1_weighted'],
        # ['unlexicalized','rnn_f1_weighted'],
        # ['gp_lexicalized','rnn_f1_weighted'],
        # ['gp_unlexicalized','rnn_f1_weighted'],
        # #  #
        # # # # #  # WR and Readability
        # ['writing_density','rnn_f1_weighted'],
        # ['writing_density_scaled','rnn_f1_weighted'],
        # # # #
        # ['readability','rnn_f1_weighted'],
        # ['readability_scaled','rnn_f1_weighted'],
        # # # #
        # # # # # # # #Phonetic Features
        # # ['phonetic','rnn_f1_weighted'],
        # ['phonetic_scores','rnn_f1_weighted'],
        # ['phonetic_scores', 'phonetic','rnn_f1_weighted'],
        # #
        # #
        # #
        # # # # # sentic concepts and scores
        # ['concepts_score','rnn_f1_weighted'],
        # ['concepts','rnn_f1_weighted'],
        # ['concepts_score', 'concepts', 'rnn_f1_weighted'],
        # # #
        #
        # #
        # # # # # sentiments
        # ['swn','rnn_f1_weighted'],
        # ['swn_batch','rnn_f1_weighted'],
        # #
        # # # # # skip grams
        #
        # #
        # # # # # stress
        # ['stress_ngrams','rnn_f1_weighted'],
        # ['stress_scores','rnn_f1_weighted'],
        # ['stress_ngrams', 'stress_scores','rnn_f1_weighted'],
        # #
        # ['two_skip_2_grams','rnn_f1_weighted'],
        # ['two_skip_3_grams','rnn_f1_weighted'],
        # #
        # ['concepts_score', 'concepts', 'book10002vec_dmm','rnn_f1_weighted'],
        # ['concepts_score', 'concepts', 'book10002vec_dbow_dmm','rnn_f1_weighted'],
        # ['concepts_score', 'concepts', 'book2vec_dbow_dmm','rnn_f1_weighted'],
        #
        # ['writing_density_scaled', 'book10002vec_dmm','rnn_f1_weighted'],
        # ['writing_density_scaled', 'book10002vec_dbow_dmm','rnn_f1_weighted'],
        # ['writing_density_scaled', 'book2vec_dbow_dmm','rnn_f1_weighted'],

        # ['readability_scaled', 'book10002vec_dmm','rnn_f1_weighted'],
        # ['readability_scaled', 'book10002vec_dbow_dmm','rnn_f1_weighted'],
        # ['readability_scaled', 'book2vec_dbow_dmm','rnn_f1_weighted'],
        # #
        # ['concepts_score', 'concepts', 'writing_density_scaled','rnn_f1_weighted'],
        # ['concepts_score', 'concepts', 'readability_scaled','rnn_f1_weighted'],
        # #
        # # # # ['writing_density_scaled', 'book10002vec_dmm', 'rnn_f1_weighted'],
        # ['concepts_score', 'concepts', 'readability_scaled', 'book10002vec_dmm'],
        # ['concepts_score', 'concepts', 'readability_scaled', 'book10002vec_dbow_dmm'],
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'book10002vec_dmm'],
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'book10002vec_dbow_dmm'],
        # #
        # ['concepts_score', 'concepts', 'readability_scaled', 'book10002vec_dmm','rnn_f1_weighted'],
        # ['concepts_score', 'concepts', 'readability_scaled', 'book10002vec_dbow_dmm','rnn_f1_weighted'],
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'book10002vec_dmm','rnn_f1_weighted'],
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'book10002vec_dbow_dmm','rnn_f1_weighted'],
        # #
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'categorical_char_ngram_whole_word','rnn_f1_weighted'],
        # ['concepts_score', 'concepts', 'readability_scaled', 'categorical_char_ngram_whole_word','rnn_f1_weighted'],
        # #
        # ['concepts_score', 'concepts', 'writing_density_scaled', 'categorical_char_ngram_mid_word','rnn_f1_weighted'],
        # ['concepts_score', 'concepts', 'readability_scaled', 'categorical_char_ngram_mid_word','rnn_f1_weighted'],
        # # # #
        # ['categorical_char_ngram_beg_punct',
        #  'categorical_char_ngram_mid_punct',
        #  'categorical_char_ngram_end_punct',
        #  'categorical_char_ngram_multi_word',
        #  'categorical_char_ngram_whole_word',
        #  'categorical_char_ngram_mid_word',
        #  'categorical_char_ngram_space_prefix',
        #  'categorical_char_ngram_space_suffix',
        #  'categorical_char_ngram_prefix',
        #  'categorical_char_ngram_suffix','rnn_f1_weighted'],
        # #
        # [
        #     'categorical_char_ngram_prefix', 'categorical_char_ngram_space_prefix',
        #     'categorical_char_ngram_space_suffix', 'categorical_char_ngram_whole_word',
        #     'categorical_char_ngram_mid_word', 'categorical_char_ngram_beg_punct', 'categorical_char_ngram_mid_punct','rnn_f1_weighted'
        # ],
        # # #
        # [
        #     'unigram', 'bigram', 'trigram', 'char_5_gram', 'categorical_char_ngram_whole_word',
        #     'two_skip_2_grams', 'lexicalized', 'google_word_emb', 'book10002vec_dbow_dmm', 'swn',
        #     'concepts_score', 'concepts','rnn_f1_weighted'
        # ],
        # [
        #     'unigram', 'bigram', 'trigram', 'char_5_gram', 'categorical_char_ngram_whole_word',
        #     'two_skip_2_grams', 'lexicalized', 'swn',
        #     'concepts_score', 'concepts','rnn_f1_weighted'
        # ]
        # ,
        # #
        #  'rnn_f1_weighted'
    ]

    VECTORS = os.path.join(basedir, '../vectors')  # 70:30 vectors genre_vectors ->genre specific stratified vectors visual_genre_vectors visual_vectors
    SUCCESS_OUTPUT = os.path.join(basedir, '../results/cover_success_few')#'../results/success/')
    GENRE_OUTPUT = os.path.join(basedir, '../results/img_text_genre')#'../results/success/')
    SUCCESS_OUTPUT_MT = os.path.join(basedir, '../results/cover_success_mt_few')#'../results/success/')
    #SUCCESS_REG_OUTPUT = os.path.join(basedir, '../results/success_regression')
    SUCCESS_REG_OUTPUT = os.path.join(basedir, '../results/img_text_success_regression')
    # SUCCESS_REG_OUTPUT=os.path.join(basedir, '../results/test_results')
    GOOGLE_EMB = '/home/sjmaharjan/Books/booksuccess/embeddings/GoogleNews-vectors-negative300.bin'
    GUTENBERG_EMB = '/home/sjmaharjan/Books/booksuccess/embeddings/gutenbergword2vec.txt'
    BOOK2VEC = '/home/sjmaharjan/Books/booksuccess/bookvectors/book_all'
    BOOK10002VEC = '/home/sjmaharjan/Books/booksuccess/bookvectors/book_1000'
    BOOK_META_INFO = 'gutenberg_goodread_2016_match.xlsx'
    SUCCESS_OUTPUT_3_CLASS = os.path.join(basedir, '../results/success_3/remaining')

    VECTORS_EMNLP=os.path.join(basedir, '../vectors_emnlp')  # 70:30 vectors


class TestingConfig(Config):
    TESTING = True


class ProductionConfig(Config):
    pass


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
