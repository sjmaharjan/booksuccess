from keras import backend as K
from keras.engine.topology import Layer
from keras.layers import activations
from keras import initializers


__all__=['BilinearLayer']

###REF https://github.com/dapurv5/keras-neural-tensor-layer/blob/master/neural_tensor_layer.py
class BilinearLayer(Layer):
    def __init__(self, units,
                 activation=None,
                 kernel_initializer='glorot_uniform',
                 **kwargs):

        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        self.units = units  # k
        self.activation = activations.get(activation)
        self.kernel_initializer = initializers.get(kernel_initializer)
        super(BilinearLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        # W: ( units,FEATURE1_SIZE, FEATURE2_SIZE) ususally square

        assert type(input_shape) is list and len(input_shape) == 2

        self.W = self.add_weight(name="W_{:s}".format(self.name),
                                 # shape=(input_shape[1][-1],input_shape[0][-1],self.units),
                                 shape=(self.units, input_shape[0][-1], input_shape[1][-1]),
                                 initializer=self.kernel_initializer,
                                 trainable=True)
        super(BilinearLayer, self).build(input_shape)  # Be sure to call this somewhere!

    def call(self, inputs, mask=None):
        if type(inputs) is not list or len(inputs) <= 1:
            raise Exception('BilinearLayer must be called on a list of tensors '
                            '(at least 2). Got: ' + str(inputs))

        f1, f2 = inputs
        # print("F1 shape: {}".format(f1.shape.eval()))
        # print("F2 shape: {}".format(f2.shape.eval()))
        # print("F1 : {}".format(f1.eval()))
        # print("F2 : {}".format(f2.eval()))
        # print("W shape: {}".format(self.W.shape.eval()))
        #
        # print("{}*{}".format(f1.shape.eval(), self.W.shape.eval()))
        M = K.dot(f1, self.W)
        # print("result: {}\n".format(M.shape.eval()))
        # # print("M: shape {}".format(M.shape.eval()))
        # print("M:  {}".format(M.eval()))

        # print("{}*{}".format(f2.shape.eval(), M.shape.eval()))
        M = K.expand_dims(f2, axis=1) * M
        # print("result: {}\n".format(M.shape.eval()))
        # print("{}*{}".format(f2.shape.eval(), M.shape.eval()))
        # print("M:  Mul shape {}".format(M.shape.eval()))
        # print("M:  Mul {}".format(M.eval()))
        M = self.activation(K.sum(M, axis=-1))
        # print("sum result: {}\n".format(M.shape.eval()))
        # print("M: shape next {}".format(M.shape.eval()))
        # print("M:  Final {}".format(M.eval()))

        return M
        # batch_size = K.shape(e1)[0]
        # k = self.output_dim
        # # print([e1,e2])
        # #feed_forward_product = K.dot(K.concatenate([e1, e2]), self.V)
        # # print(feed_forward_product)
        # bilinear_tensor_products = [K.sum((e2 * K.dot(e1, self.W[0])) + self.b, axis=1)]
        # # print(bilinear_tensor_products)
        # for i in range(k)[1:]:
        #     btp = K.sum((e2 * K.dot(e1, self.W[i])) + self.b, axis=1)
        #     bilinear_tensor_products.append(btp)
        # result = K.tanh(
        #     K.reshape(K.concatenate(bilinear_tensor_products, axis=0), (batch_size, k)) + feed_forward_product)
        # # print(result)
        # return result

    def compute_output_shape(self, input_shape):
        batch_size = input_shape[0][0]
        return (batch_size, self.units)

    def compute_mask(self, input, input_mask=None):
        # do not pass the mask to the next layers
        return None

    def get_config(self):
        return super(BilinearLayer, self).get_config()


if __name__ == '__main__':
    import numpy as np

    layer_bilinear = BilinearLayer(units=5, activation='linear', kernel_initializer='ones')
    f1 = K.variable(np.array([[1, 2], [2, 3], [4, 5]]))
    f2 = K.variable(np.array([[2, 3, 3, 4], [4, 5, 5, 6], [5, 6, 7, 8]]))
    layer_bilinear([f1, f2])

    '''

    F1 shape: [3 2]
    F2 shape: [3 4]
    F1 : [[ 1.  2.]
     [ 2.  3.]
     [ 4.  5.]]
    F2 : [[ 2.  3.  3.  4.]
     [ 4.  5.  5.  6.]
     [ 5.  6.  7.  8.]]
    W shape: [5 2 4]
    [3 2]*[5 2 4]
    result: [3 5 4]

    M:  [[[ 3.  3.  3.  3.]
      [ 3.  3.  3.  3.]
      [ 3.  3.  3.  3.]
      [ 3.  3.  3.  3.]
      [ 3.  3.  3.  3.]]

     [[ 5.  5.  5.  5.]
      [ 5.  5.  5.  5.]
      [ 5.  5.  5.  5.]
      [ 5.  5.  5.  5.]
      [ 5.  5.  5.  5.]]

     [[ 9.  9.  9.  9.]
      [ 9.  9.  9.  9.]
      [ 9.  9.  9.  9.]
      [ 9.  9.  9.  9.]
      [ 9.  9.  9.  9.]]]
    [3 4]*[3 5 4]
    result: [3 5 4]

    [3 4]*[3 5 4]
    M:  Mul shape [3 5 4]
    M:  Mul [[[  6.   9.   9.  12.]
      [  6.   9.   9.  12.]
      [  6.   9.   9.  12.]
      [  6.   9.   9.  12.]
      [  6.   9.   9.  12.]]

     [[ 20.  25.  25.  30.]
      [ 20.  25.  25.  30.]
      [ 20.  25.  25.  30.]
      [ 20.  25.  25.  30.]
      [ 20.  25.  25.  30.]]

     [[ 45.  54.  63.  72.]
      [ 45.  54.  63.  72.]
      [ 45.  54.  63.  72.]
      [ 45.  54.  63.  72.]
      [ 45.  54.  63.  72.]]]
    sum result: [3 5]

    M: shape next [3 5]
    M:  Final [[  36.   36.   36.   36.   36.]
     [ 100.  100.  100.  100.  100.]
     [ 234.  234.  234.  234.  234.]]


     w= np.ones((4,2,5))
M=np.dot(f1,w)
M
Out[95]: 
array([[[ 3.,  3.,  3.,  3.,  3.],
        [ 3.,  3.,  3.,  3.,  3.],
        [ 3.,  3.,  3.,  3.,  3.],
        [ 3.,  3.,  3.,  3.,  3.]],
       [[ 5.,  5.,  5.,  5.,  5.],
        [ 5.,  5.,  5.,  5.,  5.],
        [ 5.,  5.,  5.,  5.,  5.],
        [ 5.,  5.,  5.,  5.,  5.]],
       [[ 9.,  9.,  9.,  9.,  9.],
        [ 9.,  9.,  9.,  9.,  9.],
        [ 9.,  9.,  9.,  9.,  9.],
        [ 9.,  9.,  9.,  9.,  9.]]])
f1
Out[96]: 
array([[1, 2],
       [2, 3],
       [4, 5]])
f1.shape
Out[97]: 
(3, 2)
W
Out[98]: 
array([[[ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.]],
       [[ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.]],
       [[ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.]],
       [[ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.]]])
w
Out[99]: 
array([[[ 1.,  1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.,  1.]],
       [[ 1.,  1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.,  1.]],
       [[ 1.,  1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.,  1.]],
       [[ 1.,  1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.,  1.]]])
f1
Out[100]: 
array([[1, 2],
       [2, 3],
       [4, 5]])
w= np.ones((5,2,4))
w
Out[102]: 
array([[[ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.]],
       [[ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.]],
       [[ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.]],
       [[ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.]],
       [[ 1.,  1.,  1.,  1.],
        [ 1.,  1.,  1.,  1.]]])
f1
Out[103]: 
array([[1, 2],
       [2, 3],
       [4, 5]])
z=np.dot(f1,w)
z.shape
Out[105]: 
(3, 5, 4)
z
Out[106]: 
array([[[ 3.,  3.,  3.,  3.],
        [ 3.,  3.,  3.,  3.],
        [ 3.,  3.,  3.,  3.],
        [ 3.,  3.,  3.,  3.],
        [ 3.,  3.,  3.,  3.]],
       [[ 5.,  5.,  5.,  5.],
        [ 5.,  5.,  5.,  5.],
        [ 5.,  5.,  5.,  5.],
        [ 5.,  5.,  5.,  5.],
        [ 5.,  5.,  5.,  5.]],
       [[ 9.,  9.,  9.,  9.],
        [ 9.,  9.,  9.,  9.],
        [ 9.,  9.,  9.,  9.],
        [ 9.,  9.,  9.,  9.],
        [ 9.,  9.,  9.,  9.]]])
f2.shape
Out[107]: 
(3, 4)
f2=f2.reshape(3,1,4)
f2
Out[109]: 
array([[[2, 3, 3, 4]],
       [[4, 5, 5, 6]],
       [[5, 6, 7, 8]]])
c=f2*z
c.shape
Out[111]: 
(3, 5, 4)
c
Out[112]: 
array([[[  6.,   9.,   9.,  12.],
        [  6.,   9.,   9.,  12.],
        [  6.,   9.,   9.,  12.],
        [  6.,   9.,   9.,  12.],
        [  6.,   9.,   9.,  12.]],
       [[ 20.,  25.,  25.,  30.],
        [ 20.,  25.,  25.,  30.],
        [ 20.,  25.,  25.,  30.],
        [ 20.,  25.,  25.,  30.],
        [ 20.,  25.,  25.,  30.]],
       [[ 45.,  54.,  63.,  72.],
        [ 45.,  54.,  63.,  72.],
        [ 45.,  54.,  63.,  72.],
        [ 45.,  54.,  63.,  72.],
        [ 45.,  54.,  63.,  72.]]])
k=np.sum(c, axis=-1)
k
Out[114]: 
array([[  36.,   36.,   36.,   36.,   36.],
       [ 100.,  100.,  100.,  100.,  100.],
       [ 234.,  234.,  234.,  234.,  234.]])
k.shape
Out[115]: 
(3, 5)


    '''