import joblib
from collections import defaultdict
import os
import scipy.sparse as sp
import numpy as np


class Dataset:
    IMAGE_FEATURE_DUMP = '/uhpc/solorio/suraj/Books/visual_vectors/'
    TEXT_FEATURE_DUMP = '/uhpc/solorio/suraj/Books/vectors/'

    def __init__(self, features, label_extractor, genre=True):
        self.features = features
        self.label_extractor = label_extractor
        self.data = self.load_data()
        self.use_genre=genre
        self.genres=["Fiction", "Science_fiction", "Detective_and_mystery_stories", "Poetry", "Short_stories",
                 "Love_stories", "Historical_fiction", "Drama"]
        self.g2i= { g:i for i,g in enumerate(sorted(self.genres))}
        self.i2g={i:g for g,i in self.g2i.items() }

    def load_data(self):
        data = defaultdict(lambda: defaultdict(None))
        for feature in self.features:
            if feature in ['vgg', 'resnet']:

                X_train, Y_train_labels, Y_train_books = joblib.load(
                    self.IMAGE_FEATURE_DUMP + '{}_{}_{}.npy'.format('success', feature, 'train'))
                X_val, Y_val_labels, Y_val_books = joblib.load(
                    self.IMAGE_FEATURE_DUMP + '{}_{}_{}.npy'.format('success', feature, 'validation'))
                X_test, Y_test_labels, Y_test_books = joblib.load(
                    self.IMAGE_FEATURE_DUMP + '{}_{}_{}.npy'.format('success', feature, 'test'))

                data[feature]['X_train'] = X_train
                data[feature]['Y_train'] = Y_train_labels
                data[feature]['train_books'] = Y_train_books

                data[feature]['X_test'] = X_test
                data[feature]['Y_test'] = Y_test_labels
                data[feature]['test_books'] = Y_test_books

                data[feature]['X_val'] = X_val
                data[feature]['Y_val'] = Y_val_labels
                data[feature]['val_books'] = Y_val_books

            else:
                target_file = os.path.join(self.TEXT_FEATURE_DUMP, feature + '.pkl')
                target_index_file = os.path.join(self.TEXT_FEATURE_DUMP, feature + '_index.pkl')

                if os.path.exists(target_file):
                    X_train, X_test = joblib.load(target_file)
                    train_books, test_books = joblib.load(target_index_file)

                    data[feature]['X_train'] = X_train
                    data[feature]['train_books'] = train_books

                    data[feature]['X_test'] = X_test
                    data[feature]['test_books'] = test_books

                else:
                    raise ValueError("Feature not found")

        return data

    def get_data(self, fold='train', books=None):

        X_data = {}
        for feature in self.features:
            if feature in ['vgg', 'resnet']:
                X_data[feature] = self.data[feature]['X_' + fold]
            else:
                if fold=='val': fold='train'
                X = []
                data = self.data[feature]['X_' + fold]
                book_info = self.data[feature][fold + '_books']
                sparse = sp.issparse(data)

                for book in books:
                    if book in book_info:
                        book_index = book_info.index(book)
                        if sparse:
                            X.append(data[book_index].toarray()[0])
                        else:
                            X.append(data[book_index])
                    else:
                        # this should not happen
                        raise ValueError("Test book not found")

                X = np.array(X)

                X_data[feature] = X
        return X_data

    def get_training_data(self):
        Y_train = {}

        if 'vgg' in self.features:
            train_books = self.data['vgg']['train_books']
            train_book_Ys = self.data['vgg']['Y_train']
        else:
            train_books = self.data['resnet']['train_books']
            train_book_Ys = self.data['vgg']['Y_train']

        X_train = self.get_data(fold='train', books=train_books)

        if self.use_genre:
            X_train['genre']=np.array([[self.g2i[genre]] for genre in train_book_Ys[:, 1].ravel()])

        Y_train[self.label_extractor + '_output'] = self.get_labels(train_book_Ys)

        return X_train, Y_train

    def get_test_data(self):

        Y_test = {}

        if 'vgg' in self.features:
            test_books = self.data['vgg']['test_books']
            test_book_Ys = self.data['vgg']['Y_test']
        else:
            test_books = self.data['resnet']['test_books']
            test_book_Ys = self.data['vgg']['Y_test']

        X_test = self.get_data(fold='test', books=test_books)
        if self.use_genre:
            X_test['genre']=np.array([[self.g2i[genre]] for genre in test_book_Ys[:, 1].ravel()])

        Y_test[self.label_extractor + '_output'] = self.get_labels(test_book_Ys)

        return X_test, Y_test

    def get_validaiontion_data(self):
        Y_val = {}

        if 'vgg' in self.features:
            val_books = self.data['vgg']['val_books']
            val_book_Ys = self.data['vgg']['Y_val']
        else:
            val_books = self.data['resnet']['val_books']
            val_book_Ys = self.data['vgg']['Y_val']

        X_val = self.get_data(fold='val', books=val_books)
        if self.use_genre:
            X_val['genre']=np.array([[self.g2i[genre]] for genre in val_book_Ys[:, 1].ravel()])

        Y_val[self.label_extractor + '_output'] = self.get_labels(val_book_Ys)

        return X_val, Y_val

    def get_labels(self, Ys):
        if self.label_extractor == 'success':
            return Ys[:, 0]
        elif self.label_extractor == 'genre':
            return Ys[:, 1]
        elif self.label_extractor == 'rating':
            return Ys[:, 2]
        elif self.lable_extractor == 'success_genre':
            return Ys[:, 0], Ys[:, 1]

        else:
            raise ValueError("Lable not found")
