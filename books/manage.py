# -*- coding: utf-8 -*-
from __future__ import print_function
import codecs
from collections import defaultdict

import click
from config import config
import os
import sys
import logging
import time
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
# Set the path

basedir = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__))))

app = config[os.getenv('BOOKS_CONFIG') or 'default']


@click.group()
def manager():
    pass




@manager.command()
def profile_function():
        from readers.book import Book
        from features import create_feature
        bk=Book(book_path='/home/sjmaharjan/Books/booksuccess/data/Historical_fiction/success/82_ivanhoe.txt', book_id='82_ivanhoe.txt', genre='Historical_fiction', success=1, avg_rating=3.5, sentic_file='/home/sjmaharjan/Books/booksuccess/data/Historical_fiction/sentic/82_ivanhoe_st_parser.txt.json', stanford_parse_file='/home/sjmaharjan/Books/booksuccess/data/Historical_fiction/st_parser/82_ivanhoe_st_parser.txt')
        for i, feature in enumerate(app.FEATURES):
            ts = time.time()
            #print ("Feature name: {}".format(feature))
            f_name,f_obj=create_feature(feature)
            f_obj.fit_transform([bk,bk])

            te = time.time()

            print ('Feature name: %s, %2.2f sec,%d' % ( feature,te-ts,len(f_obj.get_feature_names())))








#################################################################################
#  Goodreads Controllers
######################################################################################

@manager.command()
def dump_vectors():
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from features.utils import fetch_features_vectorized

    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file)
    print("Done loading data")
    for i, feature in enumerate(app.FEATURES):
        print("Running Feature Extraction for feature  {}".format(feature))
        fetch_features_vectorized(data_dir=app.VECTORS, features=feature, corpus=goodreadscorpus)
        print('Done {}/{}'.format(i + 1, len(app.FEATURES)))
    print('Done....')


@manager.command()
def test_fetch_feature_vectorize():
    from features.utils import test_fetch_features_vectorizer
    test_fetch_features_vectorizer(data_dir=app.VECTORS)
    print('Done....')




@manager.command()
def prepare_train_val_test():
    import json
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from readers.book import genre_le
    import pickle

    def to_dict(d):
        if isinstance(d, defaultdict):
            return dict((k, to_dict(v)) for k, v in d.items())
        return d


    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile_train_val_test(reader=goodread_reader, split_file=split_file)
    #print (goodreadscorpus)
    #pickle.dump(dict(goodreadscorpus), open('success_splits_info.pkl','wb'))
    with open('success_splits_info.json', 'w') as outfile:
        #yaml.dump(to_dict(goodreadscorpus), outfile,default_flow_style=False)
        json.dump(to_dict(goodreadscorpus), outfile,indent=4)

    goodreadscorpus_genre = Corpus.with_splits_train_val_test(reader=goodread_reader, label_extractor=genre_le)
    #pickle.dump(dict(goodreadscorpus_genre), open('genre_splits_info.pkl', 'wb'))

    with open('genre_splits_info.json', 'w') as outfile:
        # yaml.dump(to_dict(goodreadscorpus_genre), outfile,default_flow_style=False)
        json.dump(to_dict(goodreadscorpus_genre), outfile,indent=4)



    print('Done....')

@manager.command()
def run_success():
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from experiments.success import success_classification

    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file)
    print("Done loading data")
    for i, feature in enumerate(app.FEATURES):
        print("Running Success Experiment for feature  {}".format(feature))
        success_classification(corpus=goodreadscorpus, feature=feature, dump_dir=app.VECTORS,ignore_lst=None)
        print('Done {}/{}'.format(i + 1, len(app.FEATURES)))
    print('Done....')



@manager.command()
def run_success_with_ignore_books():
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from experiments.success import success_classification

    train_igonre_lst = ["16990_the+story+of+troy.txt",
                        "17027_return+to+pleasure+island.txt",
                        "17091_oliver+cromwell.txt",
                        "22475_tortoises.txt",
                        "23431_naughty+puppies.txt",
                        "23538_hugh+selwyn+mauberley.txt",
                        "24349_coming+home+1916.txt",
                        "24679_lilith+the+legend+of+the+first+woman.txt",
                        "26772_a+question+of+courage.txt",
                        "328_a+heap+o'+livin'.txt",
                        "574_poems+of+william+blake.txt",
                        "6786_the+piccolomini.txt",
                        "6790_demetrius.txt",
                        "9989_bees+in+amber+a+little.txt"]

    test_ignore_lst = ["10460_when+day+is+done.txt",
                       "11689_are+women+people+a.txt",
                       "19224_the+alchemist's+secret.txt",
                       "24262_antinous+a+poem.txt",
                       "30062_the+plague.txt",
                       "6763_poetics+english.txt",
                       "8784_the+divine+comedy+by+dante+illustrated+hell.txt"]

    ignore_lst=train_igonre_lst+test_ignore_lst

    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file)
    print("Done loading data")
    for i, feature in enumerate(app.FEATURES):
        print("Running Success Experiment for feature  {}".format(feature))
        success_classification(corpus=goodreadscorpus, feature=feature, dump_dir=app.VECTORS, ignore_lst=ignore_lst)
        print('Done {}/{}'.format(i + 1, len(app.FEATURES)))
    print('Done....')


@manager.command()
def run_genre():
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from readers.book import genre_le
    from experiments.genre import genre_classification

    data_dir = os.path.join(basedir, '../data')
    #split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.with_splits(reader=goodread_reader,label_extractor=genre_le)
    print("Done loading data")
    for i, feature in enumerate(app.FEATURES):
        print("Running Genre Experiment for feature  {}".format(feature))
        genre_classification(corpus=goodreadscorpus, feature=feature, dump_dir=app.VECTORS)
        print('Done {}/{}'.format(i + 1, len(app.FEATURES)))
    print('Done....')


@manager.command()
def run_genre_with_ignore_books():
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from readers.book import genre_le
    from experiments.genre import genre_classification

    train_igonre_lst = ["16990_the+story+of+troy.txt",
                        "17027_return+to+pleasure+island.txt",
                        "17091_oliver+cromwell.txt",
                        "22475_tortoises.txt",
                        "23431_naughty+puppies.txt",
                        "23538_hugh+selwyn+mauberley.txt",
                        "24349_coming+home+1916.txt",
                        "24679_lilith+the+legend+of+the+first+woman.txt",
                        "26772_a+question+of+courage.txt",
                        "328_a+heap+o'+livin'.txt",
                        "574_poems+of+william+blake.txt",
                        "6786_the+piccolomini.txt",
                        "6790_demetrius.txt",
                        "9989_bees+in+amber+a+little.txt"]

    test_ignore_lst = ["10460_when+day+is+done.txt",
                       "11689_are+women+people+a.txt",
                       "19224_the+alchemist's+secret.txt",
                       "24262_antinous+a+poem.txt",
                       "30062_the+plague.txt",
                       "6763_poetics+english.txt",
                       "8784_the+divine+comedy+by+dante+illustrated+hell.txt"]

    ignore_lst = train_igonre_lst + test_ignore_lst

    data_dir = os.path.join(basedir, '../data')
    #split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.with_splits(reader=goodread_reader,label_extractor=genre_le)
    print("Done loading data")
    for i, feature in enumerate(app.FEATURES):
        print("Running Genre Experiment for feature  {}".format(feature))
        genre_classification(corpus=goodreadscorpus, feature=feature, dump_dir=app.VECTORS, ignore_lst=ignore_lst)
        print('Done {}/{}'.format(i + 1, len(app.FEATURES)))
    print('Done....')


@manager.command()
def run_success_multitask():
    from readers.corpus import Corpus
    from readers.book import genre_success_le
    from readers.goodreads import GoodreadsReader
    from experiments.success import success_classification_multitask

    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file,label_extractor=genre_success_le)
    print("Done loading data")
    for i, feature in enumerate(app.FEATURES):
        print("Running Success Experiment for feature  {}".format(feature))
        success_classification_multitask(corpus=goodreadscorpus, feature=feature, dump_dir=app.VECTORS)
        print('Done {}/{}'.format(i + 1, len(app.FEATURES)))
    print('Done....')





@manager.command()
def run_success_multitask_with_ignore_books():
    from readers.corpus import Corpus
    from readers.book import genre_success_le
    from readers.goodreads import GoodreadsReader
    from experiments.success import success_classification_multitask

    train_igonre_lst = ["16990_the+story+of+troy.txt",
                        "17027_return+to+pleasure+island.txt",
                        "17091_oliver+cromwell.txt",
                        "22475_tortoises.txt",
                        "23431_naughty+puppies.txt",
                        "23538_hugh+selwyn+mauberley.txt",
                        "24349_coming+home+1916.txt",
                        "24679_lilith+the+legend+of+the+first+woman.txt",
                        "26772_a+question+of+courage.txt",
                        "328_a+heap+o'+livin'.txt",
                        "574_poems+of+william+blake.txt",
                        "6786_the+piccolomini.txt",
                        "6790_demetrius.txt",
                        "9989_bees+in+amber+a+little.txt"]

    test_ignore_lst = ["10460_when+day+is+done.txt",
                       "11689_are+women+people+a.txt",
                       "19224_the+alchemist's+secret.txt",
                       "24262_antinous+a+poem.txt",
                       "30062_the+plague.txt",
                       "6763_poetics+english.txt",
                       "8784_the+divine+comedy+by+dante+illustrated+hell.txt"]

    ignore_lst = train_igonre_lst + test_ignore_lst

    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file,label_extractor=genre_success_le)
    print("Done loading data")
    for i, feature in enumerate(app.FEATURES):
        print("Running Success Experiment for feature  {}".format(feature))
        success_classification_multitask(corpus=goodreadscorpus, feature=feature, dump_dir=app.VECTORS, ignore_lst=ignore_lst)
        print('Done {}/{}'.format(i + 1, len(app.FEATURES)))
    print('Done....')


@manager.command()
def run_success_3_class():
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from experiments.success import success_3_class_classification
    from readers.book import success_3class_le
    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file, label_extractor=success_3class_le)
    print("Done loading data")
    for i, feature in enumerate(app.FEATURES):
        print("Running Success Experiment for feature  {}".format(feature))
        success_3_class_classification(corpus=goodreadscorpus, feature=feature, dump_dir=app.VECTORS)
        print('Done {}/{}'.format(i + 1, len(app.FEATURES)))
    print('Done....')


@manager.command()
def run_success_regression():
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from experiments.success import success_regression
    from readers.book import avg_rating_le

    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file,
                                            label_extractor=avg_rating_le)
    print("Done loading data")
    for i, feature in enumerate(app.FEATURES):
        print("Running Success Experiment for feature  {}".format(feature))
        success_regression(corpus=goodreadscorpus, feature=feature, dump_dir=app.VECTORS)
        print('Done {}/{}'.format(i + 1, len(app.FEATURES)))
    print('Done....')

@manager.command()
@click.option('--input', help='/path/to/input/directory')
@click.option('--output', help='/path/to/model/directory')
def train_w2v(input='~/suraj/Books/booksuccess/corpus/5000_random_books',
              output='~/suraj/Books/booksuccess/embeddings'):
    from deep_learning.book2vec import word2vec_train
    word2vec_train(input, output)
    print('Done....')


@manager.command()
def mcnemar_clf_test():
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from experiments.success import get_prediction
    from metrics.stats import mcnemar_test

    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file)
    print("Done loading data")
    features = ['rnn_f1_weighted', 'book10002vec_dbow_dmm']
    Y1=get_prediction(goodreadscorpus,features[0],dump_dir=app.VECTORS)[1]
    Y2=get_prediction(goodreadscorpus,features[1],dump_dir=app.VECTORS)[1]
    print ("Mcnemar Test {}".format (mcnemar_test(Y1,Y2)))

    #results Mcnemar Test (0.5, 0.47950012218695337)


@manager.command()
def learningcurve():
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from readers.book import genre_success_le
    from features.utils import  fetch_features_vectorized
    from experiments.learning_curve import run_learning_curve
    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file,label_extractor=genre_success_le)
    print("Done loading data")
    feature='char_tri'
    X_train, Y_train,=goodreadscorpus.X_train,goodreadscorpus.Y_train
    print (Y_train[0])
    run_learning_curve(X_train,Y_train,name='Learning Curve')


    #results Mcnemar Test (0.5, 0.47950012218695337)




@manager.command()
def plot_representatoins( ):
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from readers.book import genre_le
    from features.utils import fetch_features_vectorized
    from bokeh import plotting
    import pandas as pd

    features=[
        'google_word_emb',
        #'book10002vec_dmc',
        #'book10002vec_dbow',
        #'book10002vec_dmm',

        #'book10002vec_dbow_dmm',
#        'book10002vec_dbow_dmc','gutenberg_cbow_word_emb'
        # 'rnn_f1_weighted'
    ]

    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file,
                                            label_extractor=genre_le)




    for feature in features:
        X_test, Y_test, _, _ = fetch_features_vectorized(app.VECTORS, feature, goodreadscorpus)
        for algo in ['pca', 'tsne']:

            plotting.output_file(feature+'_train_'+algo + ".html")
            # p = plotting.figure(plot_width=400, plot_height=400)
            p = plotting.figure(plot_width=800, plot_height=400,
                                toolbar_location="above")

            if algo=='pca':
                from sklearn.decomposition import PCA
                model=PCA(n_components=2)
            elif algo=='tsne':
                from sklearn.manifold import TSNE
                model = TSNE(n_components=2, random_state=0)

            else:
                raise NotImplementedError('Alogrithm not implemented')


            vectors=model.fit_transform(X_test)
            df= pd.DataFrame(vectors,columns=['x','y'])
            df['label']=Y_test
            df['color']='white'
            #change color
            df['color'][df['label']=='Poetry']='green'
            df['color'][df['label']=='Historical_fiction']='yellow'
            df['color'][df['label']=='Drama']='red'
            df['color'][df['label']=='Short_stories']='brown'
            df['color'][df['label']=='Science_fiction']='blue'
            df['color'][df['label']=='Fiction']='purple'
            df['color'][df['label']=='Detective_and_mystery_stories']='black'
            df['color'][df['label']=='Love_stories']='orange'

            sub_df=df
            # sub_df=df[(df['label']=='Poetry')|(df['label']=='Historical_fiction')|(df['label']=='Drama')|(df['label']=='Short_stories')]

            p.circle(sub_df['x'][sub_df['color']=='red'],sub_df['y'][sub_df['color']=='red'],color='red', fill_alpha=0.5, size=10,legend='Drama')
            p.square(sub_df['x'][sub_df['color']=='green'],sub_df['y'][sub_df['color']=='green'],color='green', fill_alpha=0.5, size=10,legend='Poetry')
            # p.circle(sub_df['x'][sub_df['color']=='blue'],sub_df['y'][sub_df['color']=='blue'],color='blue', fill_alpha=0.5, size=10,legend='Historical Fiction')
            # p.circle(sub_df['x'][sub_df['color']=='brown'],sub_df['y'][sub_df['color']=='brown'],color='brown', fill_alpha=0.5, size=10,legend='Short Stories')
            p.diamond(sub_df['x'][sub_df['color']=='blue'],sub_df['y'][sub_df['color']=='blue'],color='blue', fill_alpha=0.5, size=10,legend='Science Fiction')

            # p.circle(sub_df['x'][sub_df['color']=='purple'],sub_df['y'][sub_df['color']=='purple'],color='purple', fill_alpha=0.5, size=10,legend='Fiction')
            p.inverted_triangle(sub_df['x'][sub_df['color']=='black'],sub_df['y'][sub_df['color']=='black'],color='black', fill_alpha=0.5, size=10,legend='Detective Mystery')
            # p.circle(sub_df['x'][sub_df['color']=='orange'],sub_df['y'][sub_df['color']=='orange'],color='orange', fill_alpha=0.5, size=10,legend='Love Stories')

            plotting.save(p)



@manager.command()
def feature_correlation():
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from readers.book import avg_rating_le
    from features.utils import fetch_features_vectorized
    import joblib
    import os
    import pandas as pd
    import numpy as np
    from metrics.stats import mutual_information, maximal_information_coefficient

    def apply_pairwise(df, func):
        l = len(df.columns)
        results = np.zeros((l, l))
        for i, ac in enumerate(df):
            for j, bc in enumerate(df):
                if j > i:
                    break
                results[i, j] = func(df.ix[:, ac].values, df.ix[:, bc].values)
        return pd.DataFrame(results, index=df.columns, columns=df.columns)

    #features=['writing_density','concepts_score']
    features=['readability']

    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file,
                                            label_extractor=avg_rating_le)
    for feature in features:
        X_train, Y_train, _, _ = fetch_features_vectorized(app.VECTORS, feature, goodreadscorpus)
        vetorizer=joblib.load(os.path.join(app.VECTORS,feature + '_model.pkl'))
        feature_names=vetorizer.get_feature_names()
        df = pd.DataFrame(X_train, columns=feature_names)
        df['labels'] = Y_train
        # change to loop later
        result_df = apply_pairwise(df, mutual_information)
        result_df.to_csv(feature + '_mi.tsv', delimeter='\t')
        result_df = apply_pairwise(df, maximal_information_coefficient)
        result_df.to_csv(feature + '_mic.tsv', delimeter='\t', encoding='utf-8')
        df.corr().to_csv(feature + '_pearson.tsv', delimeter='\t', encoding='utf-8')
        print("Done")




######################################################################################
#  EMNLP Controllers
######################################################################################

@manager.command()
def dump_vectors_emnlp13():
    from readers.corpus import Corpus
    from readers.emnlp13_dataset import EMNLP13Reader
    from features.utils import fetch_features_vectorized

    data_dir = os.path.join(basedir, '../emnlp13_data')
    split_file = os.path.join(basedir, '../emnlp13_data/train_test_split_EMNLP13.yaml')
    emnlp_reader = EMNLP13Reader(dirname=data_dir)
    emnlpcorpus = Corpus.from_splitfile(reader=emnlp_reader, split_file=split_file)
    print("Done loading data")
    for i, feature in enumerate(app.FEATURES):
        print("Running Feature Extraction for feature  {}".format(feature))
        fetch_features_vectorized(data_dir=app.VECTORS_EMNLP, features=feature, corpus=emnlpcorpus)
        print('Done {}/{}'.format(i + 1, len(app.FEATURES)))
    print('Done....')



@manager.command()
def run_success_emnlp13():
    from readers.corpus import Corpus
    from readers.emnlp13_dataset import EMNLP13Reader
    from experiments.success import success_classification

    data_dir = os.path.join(basedir, '../emnlp13_data')
    split_file = os.path.join(basedir, '../emnlp13_data/train_test_split_EMNLP13.yaml')
    emnlp_reader = EMNLP13Reader(dirname=data_dir)
    emnlpcorpus = Corpus.from_splitfile(reader=emnlp_reader, split_file=split_file)
    print("Done loading data")
    for i, feature in enumerate(app.FEATURES):
        print("Running Success Experiment for feature  {}".format(feature))
        success_classification(corpus=emnlpcorpus, feature=feature, dump_dir=app.VECTORS_EMNLP)
        print('Done {}/{}'.format(i + 1, len(app.FEATURES)))
    print('Done....')



@manager.command()
def check_train_test_avg():
    from readers.corpus import Corpus
    from readers.goodreads import GoodreadsReader
    from experiments.success import success_regression
    from readers.book import avg_rating_le

    data_dir = os.path.join(basedir, '../data')
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')
    goodread_reader = GoodreadsReader(dirname=data_dir)
    goodreadscorpus = Corpus.from_splitfile(reader=goodread_reader, split_file=split_file,
                                            label_extractor=avg_rating_le)

    print (goodreadscorpus.Y_train)
    #
    # import yaml
    # with open('rating_file.yaml', 'w') as f_out:
    #     test_data=[{x.book_id: round(y,3)} for x, y in zip(goodreadscorpus.X_test, goodreadscorpus.Y_test.tolist())]
    #     train_data=[{x.book_id: round(y,3)} for x, y in zip(goodreadscorpus.X_train, goodreadscorpus.Y_train.tolist())]
    #     new_data={'test':test_data ,
    #                'train': train_data}
    #
    #     # print (new_data)
    #     yaml.dump(new_data,f_out, default_flow_style=False)
    #
    # split_file = os.path.join(basedir, '../data/train_test_split_goodreads_avg_rating.yaml')
    #
    # import yaml
    # with open(split_file, 'r') as f:
    #     data = yaml.load(f)
    #
    # test_dict= {}
    # for ele in data['test']:
    #     test_dict.update(ele)
    #
    # test_cor_dic={}
    # for ele in new_data['test']:
    #     test_cor_dic.update(ele)
    #
    # print ('checking test rating')
    # for key,val in test_dict.items():
    #     if not (test_dict[key]==test_cor_dic[key]):
    #         print ('error')
    #
    # train_dict = {}
    # for ele in data['train']:
    #     test_dict.update(ele)
    #
    # train_cor_dic = {}
    # for ele in new_data['train']:
    #     train_cor_dic.update(ele)
    #
    #
    # print ('checking train rating')
    #
    # for key, val in train_dict.items():
    #     if not (train_dict[key] == train_cor_dic[key]):
    #         print('error')


    print("Done loading data")


@manager.command()
def prepare_train_test():
    import pandas as pd
    import codecs

    book_df = pd.read_excel(app.BOOK_META_INFO)
    split_file = os.path.join(basedir, '../data/train_test_split_goodreads.yaml')

    import yaml
    with open(split_file, 'r') as f:
        data = yaml.load(f)

    result = {}
    for partition, books in data.items():
        result[partition] = []
        for book in books:
            avg_rating = book_df[book_df['FILENAME'] == book]['AVG_RATING_2016'].values[0]

            result[partition].append({book: round(avg_rating, 3)})

    # print (result)
    with codecs.open('train_test_split_goodreads_avg_rating.yaml', 'w', encoding='utf-8') as f_out:
        yaml.dump(result, f_out, default_flow_style=False)


@manager.command()
@click.option('--input', help='/path/to/result/directory')
def collect_all_results(input):
    '''
    interface to collect results
    :param input:
    :return:
    '''
    from experiments.utils import collect_results

    order=app.FEATURES

    # collect_results(order,input,'clf')
    collect_results(order,input,'clf')

@manager.command()
def duplicated():

   import yaml
   with open('/Users/suraj/BookSuccess/booksuccess/data/genre_books.yaml','r') as f:
       data= yaml.load(f)

   print (len(set(data['test'])))
   print (len(set(data['train'])))
   print(set(data['test']).intersection(set(data['train'])))
   new_test= set(data['test'])-set(data['train'])
   new_train=set(data['train'])

   with codecs.open('train_test_split_goodreads.yaml','w',encoding='utf-8') as f_out:
    yaml.dump({'test':list(new_test), 'train':list(new_train)},f_out,default_flow_style=False)




@manager.command()
@click.option('--input', help='/path/to/result/directory')
def collect_results(input):
    from collections import defaultdict
    results = defaultdict(str)
    first = True
    for file in sorted(os.listdir(input)):
        if file.endswith('.txt'):
            with open(os.path.join(input, file), 'r') as f_in:
                lines = list(f_in.readlines())
                if len(lines) >= 2:
                    if first:
                        header = lines[-2].strip()
                        first = False
                    exp_name=lines[0].strip('\r\n').split(':')[-1].replace('[','"').replace(']','"')+','+file.split('.')[0].split('_')[-1]
                    result = lines[-1].strip()

                    results[exp_name] = result

    print(" Classifiers,  " + header)

    for model, r in results.items():
            print('{},{}'.format( model, r))
    print()



@manager.command()
@click.option('--input', help='/path/to/result/directory')
def collect_genre_results(input):
    from collections import defaultdict
    results = defaultdict(str)
    first = True
    for file in sorted(os.listdir(input)):
        if file.endswith('.txt'):
            with open(os.path.join(input, file), 'r') as f_in:
                lines = list(f_in.readlines())
                if len(lines) >= 2:
                    if first:
                        header = lines[-2].strip()
                        first = False

                    exp_name=",".join(file.split('.')[0].rsplit('_',1))
                    result = lines[-1].strip()

                    results[exp_name] = result

    print(" Classifiers,  " + header)

    for model, r in results.items():
            print('{},{}'.format( model, r))
    print()

@manager.command()
@click.option('--image_dir', help='/path/to/image/directory')
@click.option('--output_dir', help='/path/to/output/directory')
def copy_images(image_dir,output_dir):
    from utils import move_images
    move_images(image_dir,output_dir)


if __name__ == "__main__":
    manager()
