Running Experiment for Feature: ['concepts_score', 'concepts', 'book2vec_dbow_dmm']
============================================================
Training size: 694
Test size: 290
X Train Feature matrix (694, 18736)
X Test Feature matrix (290, 18736)
Fitting the classifier to the training set
Best score: 0.628
Best parameters set:
	C: 0.0001
0.522+/-0.00 {'C': 1e-05}
0.628+/-0.03 {'C': 0.0001}
0.595+/-0.03 {'C': 0.001}
0.591+/-0.02 {'C': 0.01}
0.589+/-0.02 {'C': 0.1}
0.588+/-0.02 {'C': 1}
0.589+/-0.02 {'C': 10}
0.588+/-0.02 {'C': 100}
0.589+/-0.02 {'C': 1000}
0.590+/-0.02 {'C': 10000}
Done grid search
============================================================
Predicting on the test set
Classifation Report
Training Accuracy =83.42939481268012
Test Accuracy =70.6896551724138
             precision    recall  f1-score   support

    failure       0.66      0.37      0.47       103
    success       0.72      0.89      0.80       187

avg / total       0.70      0.71      0.68       290


Confusion matrix
============================================================
['failure', 'success']

[[ 38  65]
 [ 20 167]]

Accuracy,ROC_AUC,Precision-Macro,Precision-Micro,Precision-Weighted,Recall-Macro,Recall-Micro,Recall-Weighted,F1-Macro,F1-Micro,F1-Weighted,Failure-Precision,Failure-Recall,Failure-f1,Success-Precision,Success-Recall,Success-F1
70.6896551724138,0.6309900835885986,0.6875,0.7068965517241379,0.6968638525564803,0.6309900835885988,0.7068965517241379,0.7068965517241379,0.6345928638135757,0.7068965517241379,0.6816743350111512,0.6551724137931034,0.36893203883495146,0.4720496894409938,0.7198275862068966,0.893048128342246,0.7971360381861576
