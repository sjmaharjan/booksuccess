Running Experiment for Feature: categorical_char_ngram_beg_punct
============================================================
Training size: 694
Test size: 290
X Train Feature matrix (694, 2303)
X Test Feature matrix (290, 2303)
Fitting the classifier to the training set
Best score: 0.618
Best parameters set:
	C: 10
0.517+/-0.00 {'C': 1e-05}
0.517+/-0.00 {'C': 0.0001}
0.517+/-0.00 {'C': 0.001}
0.517+/-0.00 {'C': 0.01}
0.572+/-0.02 {'C': 0.1}
0.615+/-0.03 {'C': 1}
0.618+/-0.03 {'C': 10}
0.599+/-0.03 {'C': 100}
0.599+/-0.03 {'C': 1000}
0.597+/-0.03 {'C': 10000}
Done grid search
============================================================
Predicting on the test set
Classifation Report
Training Accuracy =97.9827089337
Test Accuracy =64.1379310345
             precision    recall  f1-score   support

    failure       0.49      0.33      0.40       103
    success       0.69      0.81      0.75       187

avg / total       0.62      0.64      0.62       290


Confusion matrix
============================================================
['failure', 'success']

[[ 34  69]
 [ 35 152]]

Accuracy,ROC_AUC,Precision-Macro,Precision-Micro,Precision-Weighted,Recall-Macro,Recall-Micro,Recall-Weighted,F1-Macro,F1-Micro,F1-Weighted,Failure-Precision,Failure-Recall,Failure-f1,Success-Precision,Success-Recall,Success-F1
64.1379310345,0.571465655989,0.590268214309,0.641379310345,0.618513820013,0.571465655989,0.641379310345,0.641379310345,0.570223438212,0.641379310345,0.620876770917,0.492753623188,0.330097087379,0.395348837209,0.68778280543,0.812834224599,0.745098039216
