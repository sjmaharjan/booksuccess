Running Experiment for Feature: ['bigram', 'trigram']
============================================================
Training size: 694
Test size: 290
X Train Feature matrix (694, 1507649)
X Test Feature matrix (290, 1507649)
Fitting the classifier to the training set
Best score: 0.617
Best parameters set:
	C: 10
0.517+/-0.00 {'C': 1e-05}
0.517+/-0.00 {'C': 0.0001}
0.517+/-0.00 {'C': 0.001}
0.517+/-0.00 {'C': 0.01}
0.580+/-0.03 {'C': 0.1}
0.617+/-0.04 {'C': 1}
0.617+/-0.04 {'C': 10}
0.613+/-0.04 {'C': 100}
0.613+/-0.04 {'C': 1000}
0.613+/-0.04 {'C': 10000}
Done grid search
============================================================
Predicting on the test set
Classifation Report
Training Accuracy =100.0
Test Accuracy =68.6206896552
             precision    recall  f1-score   support

    failure       0.61      0.33      0.43       103
    success       0.71      0.88      0.78       187

avg / total       0.67      0.69      0.66       290


Confusion matrix
============================================================
['failure', 'success']

[[ 34  69]
 [ 22 165]]

Accuracy,ROC_AUC,Precision-Macro,Precision-Micro,Precision-Weighted,Recall-Macro,Recall-Micro,Recall-Weighted,F1-Macro,F1-Micro,F1-Weighted,Failure-Precision,Failure-Recall,Failure-f1,Success-Precision,Success-Recall,Success-F1
68.6206896552,0.606225014278,0.656135531136,0.686206896552,0.670326512568,0.606225014278,0.686206896552,0.686206896552,0.605760468486,0.686206896552,0.657344437627,0.607142857143,0.330097087379,0.427672955975,0.705128205128,0.882352941176,0.783847980998
