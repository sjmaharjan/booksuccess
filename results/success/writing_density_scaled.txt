Running Experiment for Feature: writing_density_scaled
============================================================
Training size: 694
Test size: 290
X Train Feature matrix (694, 9)
X Test Feature matrix (290, 9)
Fitting the classifier to the training set
Best score: 0.605
Best parameters set:
	C: 0.1
0.517+/-0.00 {'C': 1e-05}
0.517+/-0.00 {'C': 0.0001}
0.538+/-0.01 {'C': 0.001}
0.567+/-0.01 {'C': 0.01}
0.605+/-0.02 {'C': 0.1}
0.506+/-0.05 {'C': 1}
0.442+/-0.05 {'C': 10}
0.443+/-0.05 {'C': 100}
0.470+/-0.03 {'C': 1000}
0.517+/-0.00 {'C': 10000}
Done grid search
============================================================
Predicting on the test set
Classifation Report
Training Accuracy =70.8933717579
Test Accuracy =66.2068965517
             precision    recall  f1-score   support

    failure       0.57      0.19      0.29       103
    success       0.67      0.92      0.78       187

avg / total       0.64      0.66      0.60       290


Confusion matrix
============================================================
['failure', 'success']

[[ 20  83]
 [ 15 172]]

Accuracy,ROC_AUC,Precision-Macro,Precision-Micro,Precision-Weighted,Recall-Macro,Recall-Micro,Recall-Weighted,F1-Macro,F1-Micro,F1-Weighted,Failure-Precision,Failure-Recall,Failure-f1,Success-Precision,Success-Recall,Success-F1
66.2068965517,0.556980426769,0.622969187675,0.662068965517,0.63789819376,0.556980426769,0.662068965517,0.662068965517,0.534067807725,0.662068965517,0.604805289663,0.571428571429,0.194174757282,0.289855072464,0.674509803922,0.919786096257,0.778280542986
