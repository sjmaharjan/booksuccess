Running Experiment for Feature: ['categorical_char_ngram_beg_punct', 'categorical_char_ngram_mid_punct', 'categorical_char_ngram_end_punct', 'categorical_char_ngram_multi_word', 'categorical_char_ngram_whole_word', 'categorical_char_ngram_mid_word', 'categorical_char_ngram_space_prefix', 'categorical_char_ngram_space_suffix', 'categorical_char_ngram_prefix', 'categorical_char_ngram_suffix']
============================================================
Training size: 694
Test size: 290
X Train Feature matrix (694, 33721)
X Test Feature matrix (290, 33721)
Fitting the classifier to the training set
Best score: 0.620
Best parameters set:
	C: 1000
0.517+/-0.00 {'C': 1e-05}
0.517+/-0.00 {'C': 0.0001}
0.517+/-0.00 {'C': 0.001}
0.554+/-0.02 {'C': 0.01}
0.604+/-0.03 {'C': 0.1}
0.610+/-0.03 {'C': 1}
0.609+/-0.02 {'C': 10}
0.612+/-0.02 {'C': 100}
0.620+/-0.02 {'C': 1000}
0.620+/-0.02 {'C': 10000}
Done grid search
============================================================
Predicting on the test set
Classifation Report
Training Accuracy =100.0
Test Accuracy =67.5862068966
             precision    recall  f1-score   support

    failure       0.57      0.38      0.45       103
    success       0.71      0.84      0.77       187

avg / total       0.66      0.68      0.66       290


Confusion matrix
============================================================
['failure', 'success']

[[ 39  64]
 [ 30 157]]

Accuracy,ROC_AUC,Precision-Macro,Precision-Micro,Precision-Weighted,Recall-Macro,Recall-Micro,Recall-Weighted,F1-Macro,F1-Micro,F1-Weighted,Failure-Precision,Failure-Recall,Failure-f1,Success-Precision,Success-Recall,Success-F1
67.5862068966,0.609106484606,0.637812315562,0.675862068966,0.658839810864,0.609106484606,0.675862068966,0.675862068966,0.611548107615,0.675862068966,0.657330927559,0.565217391304,0.378640776699,0.453488372093,0.710407239819,0.839572192513,0.769607843137
