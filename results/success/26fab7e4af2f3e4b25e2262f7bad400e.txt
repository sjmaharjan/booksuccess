Running Experiment for Feature: ['char_tri', 'char_4_gram', 'char_5_gram']
============================================================
Training size: 694
Test size: 290
X Train Feature matrix (694, 624807)
X Test Feature matrix (290, 624807)
Fitting the classifier to the training set
Best score: 0.630
Best parameters set:
	C: 10
0.517+/-0.00 {'C': 1e-05}
0.517+/-0.00 {'C': 0.0001}
0.517+/-0.00 {'C': 0.001}
0.517+/-0.00 {'C': 0.01}
0.602+/-0.04 {'C': 0.1}
0.622+/-0.04 {'C': 1}
0.630+/-0.04 {'C': 10}
0.612+/-0.03 {'C': 100}
0.604+/-0.03 {'C': 1000}
0.603+/-0.03 {'C': 10000}
Done grid search
============================================================
Predicting on the test set
Classifation Report
Training Accuracy =100.0
Test Accuracy =70.0
             precision    recall  f1-score   support

    failure       0.62      0.41      0.49       103
    success       0.73      0.86      0.79       187

avg / total       0.69      0.70      0.68       290


Confusion matrix
============================================================
['failure', 'success']

[[ 42  61]
 [ 26 161]]

Accuracy,ROC_AUC,Precision-Macro,Precision-Micro,Precision-Weighted,Recall-Macro,Recall-Micro,Recall-Weighted,F1-Macro,F1-Micro,F1-Weighted,Failure-Precision,Failure-Recall,Failure-f1,Success-Precision,Success-Recall,Success-F1
70.0,0.634364778568,0.671436142024,0.7,0.687016428193,0.634364778568,0.7,0.7,0.639257066873,0.7,0.682134431433,0.617647058824,0.407766990291,0.491228070175,0.725225225225,0.860962566845,0.78728606357
